import { performance } from "perf_hooks";
import { APlayer } from "etwin-socket-server";

export class CafejeuxPlayer extends APlayer
{
    private static readonly NB_MINUTES = 5;
    private _remainingTime = 60 * CafejeuxPlayer.NB_MINUTES;
    private timer = 0;

    public runTimer()
    {
        this.timer = performance.now();
    }
    public stopTimer()
    {
        if (this.timer === 0)
            return;
        this._remainingTime -= (performance.now() - this.timer) / 1000;
    }

    public get remainingTime():number
    {
        return (this._remainingTime);
    }
};