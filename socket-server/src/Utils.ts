type ForEachCallback<T> = (d:T|undefined, x:number, y:number) => void;

export class LinearMatrix<T>
{
    private data:T[] = [];
    
    constructor(private readonly width:number, private readonly height:number = width)
    {

    }

    public isOutsideRange(x:number, y:number):boolean
    {
        return (x >= this.width || x < 0 || y >= this.height || y < 0);
    }

    public forEach(callback:ForEachCallback<T>)
    {
        for (let x = this.width - 1; x >= 0; x--) {
            for (let y = this.height - 1; y >= 0; y--) {
                callback(this.get(x, y), x, y);
            }
        }
    }   
    public get(x:number|Vector2, y?:number):T|undefined
    {
        if (typeof x == "number" && y != undefined) {
            if (this.isOutsideRange(x, y))
                return (undefined);
            return (this.data[y * this.width + x]);
        }
        else if (typeof x != "number" && x.x != undefined && x.y != undefined) {
            if (this.isOutsideRange(x.x, x.y))
                return;
            return (this.data[x.y * this.width + x.x]);
        }
        return (undefined);
    }
    public set(x:number, y:number, value:T):void
    {
        if (this.isOutsideRange(x, y))
            return;
            this.data[y * this.width + x] = value;
    }
    public getWidth():number
    {
        return (this.width);
    }
    public getHeight():number
    {
        return (this.height);
    }
    public toString():string
    {
        let output = "";

        for (let y = 0; y < this.height; y++) {
            output += y + " = ";
            for (let x = 0; x < this.width; x++) {
                output += this.get(x, y) + " | ";
            }
            output += "\n";
        }
        return (output);
    }
}
export type Vector2<T = number> = {x:T, y:T};