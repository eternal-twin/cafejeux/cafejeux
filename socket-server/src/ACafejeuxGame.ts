import { ARealtimeGame, IReceivedPacket, PlayerId } from "etwin-socket-server/dist/src/index";
import { CafejeuxPackets } from "./CafejeuxPackets";
import { CafejeuxPlayer } from "./CafejeuxPlayer";

export abstract class ACafejeuxGame<T extends CafejeuxPlayer> extends ARealtimeGame<T>
{
    protected static readonly ROOM_CAPACITY = 2;
    private playerIds:Array<PlayerId> = [];
    private currentPlayerId = ~~(Math.random() * ACafejeuxGame.ROOM_CAPACITY);

    constructor(opts:any)
    {
        super({...opts, roomCapacity: ACafejeuxGame.ROOM_CAPACITY}, true);
        this.on("message", (packet:CafejeuxPackets.MessagePacket, emitter:T) =>
        {
            this.broadcast<CafejeuxPackets.MessagePacket>("message", {
                player: emitter.ID,
                message: packet.message
            });
        });
    }

    protected abstract getDataOnReconnection(p:T):IReceivedPacket;
    /**
     * Check that the player who sent the data matches with the one who must play.
     */
    protected checkTurnIntegrity(id:PlayerId):boolean
    {
        return (id === this.playerIds[this.currentPlayerId]);
    }
     /**
     * Stop the timer of the current player and switch to the new one.
     * It also send a "change_turn" event.
     */
    protected changeTurn()
    {
        this.currentPlayer.stopTimer();
        this.currentPlayerId = (this.currentPlayerId + 1) % ACafejeuxGame.ROOM_CAPACITY;
        this.currentPlayer.runTimer();
        this.broadcast("change_turn", <CafejeuxPackets.ChangeTurnPacket>{
            current: this.playerIds[this.currentPlayerId],
            remainingTime: this.currentPlayer.remainingTime
        });
    }
    protected onJoin(player:T)
    {
        this.playerIds.push(player.ID);
        this.broadcast<CafejeuxPackets.Join>("join", { player: player.ID });
    }
    protected onReconnect(player:T)
    {
        player.send("reconnect", this.getDataOnReconnection(player));
    }
    protected onDisconnect(player:T)
    {
        this.broadcast<CafejeuxPackets.Leave>("leave", { player: player.ID });
    }

    public get currentPlayer():T
    {
        return (this.getPlayer(this.playerIds[this.currentPlayerId])!);
    }
};