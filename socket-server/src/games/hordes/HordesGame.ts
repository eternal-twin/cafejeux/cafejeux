import { ACafejeuxGame } from "./../../ACafejeuxGame";
import { HordesBoard } from "./HordesBoard";
import { HordesObjectBuilder } from "./HordesObjectBuilder";
import { HordesPackets } from "./HordesPackets";
import { HordesPlayer } from "./HordesPlayer";
import { HordesTypes } from "./HordesTypes";
import { Vector2 } from "./../../Utils";

export class HordesGame extends ACafejeuxGame<HordesPlayer>
{
    private static readonly BOARD_SIZE = 5;
    private static readonly NB_TURNS_WITHOUT_ZOMBIES = 3;
    public static readonly MIN_NB_PAWNS = 1;
    public static readonly MAX_NB_PAWNS = 7;
    private readonly board = new HordesBoard(HordesGame.BOARD_SIZE);
    private readonly colors = ["red", "blue"];
    private currentTurn = -HordesGame.NB_TURNS_WITHOUT_ZOMBIES * 2 - 1;

    constructor(opts:any)
    {
        super(opts);
        this.registerReceiveEvent("play", this.playTurn);
    }

    private playTurn = (data:HordesPackets.Play, emitter:HordesPlayer) =>
    {
        let modifiedPositions:Vector2[]|undefined;

        if (!this.checkTurnIntegrity(emitter.ID) || !data?.position)
            return;
        modifiedPositions = this.computeMove(data, emitter);
        if (!modifiedPositions)
            return;
        this.apply((p) => {
            p.send<HordesPackets.Update>("update", {
                modifiedTiles: this.board.convertPositionsToTileArray(modifiedPositions!),
                player: emitter.ID,
                current: p.current,
                next: p.next,
                object: data.object
            });
        });
        if (!emitter.canPlay)
            this.changeTurn();
    }
    private computeMove(data:HordesPackets.Play, emitter:HordesPlayer):Vector2[]|undefined
    {
        let tile:HordesTypes.Tile;
        let output:Vector2[];

        if (data.object)
            return (emitter.playObject(data.object, data.position));
        tile = {quantity: emitter.playShot(), owner: emitter.ID};
        output = this.board.play(data.position, tile);
        if (output.length === 0)
            return (undefined);
        return (output);
    }
    private computeNewTurn()
    {
        let pos:Vector2;
        let output:Vector2[];
        
        this.currentTurn++;
        if (this.currentTurn % 2 !== 0)
            return;
        if (this.currentTurn < 0) {
            return;
        }
        pos = this.board.getRandomUnoccupiedPosition();
        output = this.board.play(pos, {quantity: HordesPlayer.getRandomPawnStrength()});
        this.broadcast<HordesPackets.ZombieTurn>("zombie_turn", {
            currentTurn: this.currentTurn,
            modifiedTiles: this.board.convertPositionsToTileArray(output)
        });
    }

    protected getDataOnReconnection = (p:HordesPlayer) =>
    {
        return (<HordesPackets.Start> {
            board: this.board,
            current: p.current,
            next: p.next,
            color: p.color,
            objects: p.getObjects()
        });
    }

    public changeTurn()
    {
        this.computeNewTurn();
        if (this.board.areAllTilesOccupied) {
            this.stop();
            return;
        }
        super.changeTurn();
    }
    public run()
    {
        this.apply((p) => {
            p.color = this.colors.pop()!;
            p.objects = HordesObjectBuilder.generate(this.board, p);
            p.send("start", this.getDataOnReconnection(p));
        });
        this.computeNewTurn();
    }
    public close()
    {
        let highestScore = 0;

        this.apply((p) => {
        	p.score = this.board.getTotalPopulationQuantity(p.ID);
            if (p.score > highestScore)
                highestScore = p.score;
        });
        this.apply((p) => p.send<HordesPackets.End>("end", {
            score: p.score,
            hasWon: highestScore === p.score
        }));
    }
}