import { CafejeuxPlayer } from "./../../CafejeuxPlayer";
import { HordesGame } from "./HordesGame";
import { AHordesObject } from "./HordesObjects";
import { Vector2 } from "./../../Utils";

export class HordesPlayer extends CafejeuxPlayer
{
    private readonly pawns:number[] = [];
    private shots = 1;
    private _objects:AHordesObject[] = [];
    public color:string = "";
    public score = 0;

    constructor()
    {
        super();
        this.pawns.push(HordesPlayer.getRandomPawnStrength());
        this.pawns.push(HordesPlayer.getRandomPawnStrength());
    }

    public static getRandomPawnStrength():number
    {
        const min = HordesGame.MIN_NB_PAWNS;

        return (~~(min + (Math.random() * (HordesGame.MAX_NB_PAWNS - min + 1))));
    }

    /**
     * Use an object of the user, at the given position on the board.
     * It returns the number of cases that were affected by the object's usage.
     * In case of failure, it returns undefined.
     * Failure cases are:
     * -The object's type is not in the player's hand
     * -The object can't be used on this position.
     *
     * @params id:string => The type of object to use
     * @returns An array containging all positions of affected tiles, undefined otherwise.
     */
    public playObject(id:string, position:Vector2):Vector2[]|undefined
    {
        const objIndex = this._objects.findIndex((obj) => obj.ID === id);
        let output:Vector2[]|undefined;

        if (objIndex === -1)
            return (undefined);
        output = this._objects[objIndex].execute(position)
        if (!output)
            return (undefined);
        this._objects.splice(1, objIndex);
        return (output);
    }
    public playShot():number
    {
        this.pawns.push(HordesPlayer.getRandomPawnStrength());
        this.consumeShot();
        return (this.pawns.shift()!);
    }
    public addShot()
    {
        this.shots++;
    }
    public consumeShot()
    {
        this.shots--;
    }
    public getObjects():string[]
    {
        return (this._objects.map((obj) => obj.ID));
    }
    
    public get current():number
    {
        return (this.pawns[0]);
    }
    public get next():number
    {
        return (this.pawns[1]);
    }
    public get canPlay():boolean
    {
        return (this.shots > 0);
    }
    public set objects(objects:AHordesObject[])
    {
        this._objects = objects;
    }
}