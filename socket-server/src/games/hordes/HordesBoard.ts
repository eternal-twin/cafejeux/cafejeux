import { HordesGame } from "./HordesGame";
import { HordesTypes } from "./HordesTypes";
import { LinearMatrix, Vector2 } from "./../../Utils";
import { PlayerId } from "etwin-socket-server";

export class HordesBoard extends LinearMatrix<HordesTypes.Tile|undefined>
{
    private static readonly MIN_NB_TILES_TO_REMOVE = 4;
    private static readonly MAX_NB_TILES_TO_REMOVE = 6;
    private static readonly NB_PAWNS_KILLED_BY_TRAP = 3;
    private static readonly GAP = HordesBoard.MAX_NB_TILES_TO_REMOVE - HordesBoard.MIN_NB_TILES_TO_REMOVE;
    private readonly traps:Vector2[] = [];

    constructor(size:number)
    {
        super(size);
        this.generate();
    }

    private generate()
    {
        const positions:Vector2[] = [];
        let nbTiles = HordesBoard.MIN_NB_TILES_TO_REMOVE + ~~(Math.random() * HordesBoard.GAP);
        let selected:Vector2;

        for (let x = this.getWidth() - 1; x >= 0; x--) {
            for (let y = this.getHeight(); y >= 0; y--) {
                this.set(x, y, {quantity: 0});
                positions.push({x, y});
            }
        }
        for (; nbTiles >= 0; nbTiles--) {
            selected = positions.splice(~~(Math.random() * positions.length), 1)[0];
            this.set(selected.x, selected.y, undefined);
        }
    }

    public convertPositionsToTileArray(positions:Vector2[]):HordesTypes.ModifiedTile[]
    {
        return (positions.map((pos) => {
            return ({
                quantity: this.get(pos)?.quantity!,
                owner: this.get(pos)?.owner,
                position: pos
            });
        }));
    }
    /**
     * Returns every position around the position passed as parameter.
     * The returned values must have an hexagon shape.
     * The position passed as parameter is also returned in the array.
     * The undefined positions are not included.
     *
     * @params pos:Vector2
     * @returns An array containging every position, as an hexagon shape.
     */
    public getTilesAround(pos:Vector2):Vector2[]
    {
        const HORIZONTAL_PADDING = pos.y % 2 !== 0 ? 1 : 0;
        const RADIUS = 1;
        const output:Vector2[] = [];

        for (let x = pos.x - RADIUS; x < pos.x + RADIUS; x++) {
            for (let y = pos.y - RADIUS; y <= pos.y + RADIUS; y++) {
                output.push({x: x + HORIZONTAL_PADDING, y: y});
            }
        }
        output.push({x: pos.x + (pos.y % 2 !== 0 ? -1 : 1), y: pos.y});
        return (output.filter((pos) => this.get(pos.x, pos.y)));
    }
    /**
     * Add the given quantity to the current tile.
     * If the quantity drops to 0, owner is removed.
     * If the quantity is under 1 or over HordesGame.MAX_NB_PAWNS, it returns false.
     * If the tile is undefined, it also returns false.
     * 
     * @params quantity:number = the quantity to add.
     * @returns True if the operation is possible, false otherwise.
     */
    public addQuantityToTile(position:Vector2, quantity:number):boolean
    {
        const tile = this.get(position);

        if (!tile || tile.quantity === 0)
            return (false);
        if ((tile.quantity === HordesGame.MAX_NB_PAWNS && quantity > 0))
            return (false);
        tile.quantity = (tile.quantity + quantity) % (HordesGame.MAX_NB_PAWNS + 1);
        if (tile.quantity > 0)
            return (true);
        tile.quantity = 0;
        tile.owner = undefined;
        return (true);
    }
    /**
     * @params position:Vection2 Tre position of the trap.
     * @returns True if the tile is not undefined and unoccupied, false otherwise.
     */
    public addTrap(position:Vector2):boolean
    {
        const tile = this.get(position);

        if (!tile || tile.quantity !== 0)
            return (false);
        this.traps.push(position);
        return (true);
    }
    public getRandomUnoccupiedPosition():Vector2
    {
        const positions:Vector2[] = [];
        
        this.forEach((tile, x, y) => {
            if (tile?.quantity === 0)
                positions.push({x, y});
        });
        return (positions[~~(Math.random() * positions.length)]);
    }
    /**
     * Update a tile at the indicated position by populating it.
     * Also update every tile around and capture them if they are less populated.
     * If the owner of the captured tile was undefined, so, a zombie, the tile will be unset.
     * It will returns every position of populations that join the owner.
     * 
     * @params pos:Vector2
     * @params data:HordesTypes.Tile = The tile that will be placed on board.
     * @returns An array containing all modified positions. If it is empty, it has failed. 
     */
    public play(pos:Vector2, data:HordesTypes.Tile):Vector2[]
    {
        const output = [pos];
        const tilesAround = this.getTilesAround(pos);
        const trapIndex = this.traps.findIndex((trap) => trap.x === pos.x && trap.y === pos.y);
        let tile:HordesTypes.Tile|undefined;
        
        if (this.get(pos)?.quantity !== 0)
            return ([]);
        if (trapIndex !== -1) {
            this.traps.splice(trapIndex, 1);
            data.quantity -= HordesBoard.NB_PAWNS_KILLED_BY_TRAP;
            if (data.quantity <= 0)
                return (output);
        }
        for (const ta of tilesAround) {
            tile = this.get(ta)!;
            if (tile.quantity === 0 || tile.owner === data.owner || tile.quantity >= data.quantity)
                continue;
            if (!tile.owner)
                tile.quantity = 0;
            else
                tile.owner = data.owner;
            output.push(ta);
        }
        this.set(pos.x, pos.y, data);
        return (output);
    }
    public getTotalPopulationQuantity(id:PlayerId):number
    {
        let output = 0;

        this.forEach((tile) => {
            if (tile?.owner === id)
                output += tile.quantity;
        });
        return (output);
    }
    public get areAllTilesOccupied():boolean
    {
        return (this.getRandomUnoccupiedPosition() === undefined);
    }
}