import { PlayerId } from "etwin-socket-server";
import { Vector2 } from "../../Utils";
import { QuatcinelleGame } from "./QuatcinelleGame";

export class QuatcinelleBoard
{
    private readonly pawns:PlayerId[][] = [];

    constructor()
    {
        this.pawns.push([""]);
    }

    private getPawn(x:number, y:number):string|undefined
    {
        if (!this.pawns[x])
            return (undefined);
        return (this.pawns[x][y]);
    }
    /**
     * Check pawn alignment with (QuatcinelleGame.NB_PAWNS_FOR_WINNING - 1) other pawns of same type.
     * The direction given as parameter must hold null or positive values.
     * That's because the alignment will also be checked in the opposite directory automatically.
     *
     * @params pos:Vector2 = The position where the move is played.
     * @params dir:Vector2 = The direction in which the alignment should be checked.
     * @returns An empty array if not alignment found, else the alignment that led to victory.
     */
    private getAlignedPawns(p:Vector2, dir:Vector2):Vector2[]
    {
        const N = QuatcinelleGame.NB_PAWNS_FOR_WINNING;
        let i = p.x - (N - 1) * dir.x;
        let j = p.y - (N - 1) * dir.y;
        let output:Vector2[] = [];

        if (dir.x == 0 && dir.y == 0)
            return ([]);
        dir.x = ~~Math.abs(dir.x);
        dir.y = ~~Math.abs(dir.y);
        for (; i < p.x + N && j < p.y + N; i += dir.x, j += dir.y) {
            if (this.getPawn(p.x, p.y) == this.getPawn(i, j))
                output.push({x: i, y: j});
            else
                output = [];
            if (output.length >= N)
                return (output);
        }
        return ([]);
    }
    /**
     * Check that the played move is valid. A move is valid if:
     * - It is not overlapping another pawn.
     * - One of its side is connected to another pawn.
     *
     * @params pos:Vector2 = The position where the move is played.
     * @returns A boolean indicates whether of not the move is valid.
     */
    public isMoveValid(pos:Vector2):boolean
    {
        const dir:Vector2[] = [
            {x: -1, y: 0},
            {x: 1, y: 0},
            {x: 0, y: -1},
            {x: 0, y: 1}
        ];

        if (this.getPawn(pos.x, pos.y) != undefined)
            return (false);
        for (let d = dir.length - 1; d >= 0; d--) {
            if (this.getPawn(pos.x + dir[d].x, pos.y + dir[d].y) != undefined)
                return (true);
        }
        return (false);
    }
    public getWinningAlignment(pos:Vector2):Vector2[]
    {
        const dirs:Vector2[] = [
            {x: 1, y: 0},
            {x: 0, y: 1},
            {x: 1, y: 1}
        ];
        let output = [];
        
        for (let i = dirs.length - 1; i >= 0; i--) {
            output = this.getAlignedPawns(pos, dirs[i]);
            if (output.length >= QuatcinelleGame.NB_PAWNS_FOR_WINNING) {
                return (output);
            }
        }
        return ([]);
    }
    public getAll()
    {
        return (this.pawns);
    }
    public setPawn(x:number, y:number, pawn:PlayerId)
    {
        if (!this.pawns[x])
            this.pawns[x] = [];
        this.pawns[x][y] = pawn;
    }
}