import { ACafejeuxGame } from "./../../ACafejeuxGame";
import { ExpanzBoard } from "./ExpanzBoard";
import { ExpanzPackets } from "./ExpanzPackets";
import { ExpanzPlayer } from "./ExpanzPlayer";
import { ExpanzTypes } from "./ExpanzTypes";
import { Vector2 } from "./../../Utils";

export class ExpanzGame extends ACafejeuxGame<ExpanzPlayer>
{
	private static readonly BOARD_SIZE = 20;
	private readonly board = new ExpanzBoard(ExpanzGame.BOARD_SIZE);
	private biggestTerritory:number = 0;

    constructor(opts:any)
    {
        super(opts);
		this.registerReceiveEvent("play", this.playTurn);
	}

	private playTurn = (data:ExpanzPackets.PlayedTurn, emitter:ExpanzPlayer) =>
	{
		const colors:ExpanzTypes.Color[] = [];
		let i = 0;

		if (!this.checkTurnIntegrity(emitter.ID) || !data.position)
			return;
		this.apply((p:ExpanzPlayer) => colors.push(p.currentColor!));
		this.board.takenColors = colors;
		if (!this.board.isColorAlreadyTaken(data.position))
			return;
		if (!this.compute(data.position, emitter))
			return;
		for (; i < ACafejeuxGame.ROOM_CAPACITY; i++) {
			this.changeTurn();
			if (!this.board.isTerritoryLocked(this.currentPlayer.territory))
				break;
		}
		if (i === ACafejeuxGame.ROOM_CAPACITY) {
			this.stop();
		}
	}
	private updatePlayerTerritory(pos:Vector2, emitter:ExpanzPlayer):boolean
	{
		let territory:Vector2[] = [];

		if (!emitter.initialPosition) {
			emitter.initialPosition = pos;
			territory.push(pos);
		}
		else {
			this.board.paint(emitter.territory, emitter.currentColor!);
			territory = this.board.spread(pos, emitter.currentColor);
			if (!emitter.isTerritoryLinked(territory)) {
				this.board.paint(territory, emitter.currentColor!);
				return (false);
			}
		}
		emitter.territory = territory;
		if (territory.length > this.biggestTerritory)
			this.biggestTerritory = territory.length;
		return (true)
	}
	/**
	 * Update the player's territory and check its move is a valid move.
	 * It fires the "update" socket event if the move is valid.
	 *
	 * @params pos:Vector2 => The position where the played played.
	 * @params emitter:ExpanzPlayer => The object representing the player who played.
	 * @returns A boolean indicates whether the player's move was valid or not.
	 */
	private compute(pos:Vector2, emitter:ExpanzPlayer):boolean
	{
		if (pos.x < 0 || pos.x >= this.board.getWidth() || pos.y < 0 
			|| pos.y >= this.board.getHeight())
			return (false);
		emitter.currentColor = this.board.get(pos);
		if (!this.updatePlayerTerritory(pos, emitter))
			return (false);
		this.broadcast<ExpanzPackets.UpdateBoard>("update", {
			playerId: emitter.ID,
			color: emitter.currentColor!,
			territory: emitter.territory
		});
		return (true);
	}

	protected getDataOnReconnection = () =>
	{
		//TO DO
		return ({});
	}
	
	public run()
	{
		this.broadcast<ExpanzPackets.GetBoard>("start", { board: this.board });
	}
	public close()
	{
		this.apply((p) => {
			p.send<ExpanzPackets.End>("end", {
				hasWon: p.territory.length === this.biggestTerritory
			});
		});
	}
}