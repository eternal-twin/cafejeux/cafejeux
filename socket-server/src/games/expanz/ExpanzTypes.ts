import { PlayerId } from "etwin-socket-server";

export module ExpanzTypes
{
	export enum Color
	{
		TAKEN = -1,
		START,
		PINK,
		YELLOW,
		GREEN,
		BLUE,
		COUNT
	}
	export type Summary = {id:PlayerId, territorySize:number, hasWon:boolean};
}