import { MagmaxBoard } from "./MagmaxBoard";
import { MagmaxPlayer } from "./MagmaxPlayer";
import { MagmaxTypes } from "./MagmaxTypes";
import { Vector2 } from "./../../Utils";

export abstract class AMagmaxCard
{
    constructor(
        public readonly type:string, 
        protected board:MagmaxBoard,
        protected player:MagmaxPlayer
    )
    {}

    public abstract execute(position:Vector2, orientation?:MagmaxTypes.Orientation):boolean;
}

export class MoveSingle extends AMagmaxCard
{
    public execute(position:Vector2, orientation?:MagmaxTypes.Orientation):boolean
    {
        if (!orientation)
            return (false);
        return (this.board.move(position, orientation));
    }
}
export class Slide extends AMagmaxCard
{
    public execute(position:Vector2, orientation?:MagmaxTypes.Orientation):boolean
    {
        const dir = MagmaxBoard.transformOrientationToDirection(orientation!);
        let i = 0;
        
        if (!orientation)
            return (false);
        while (!this.board.isOutsideRange(position.x + dir.x, position.y + dir.y)) {
            if (!this.board.move(position, orientation))
                break;
            position.x += dir.x;
            position.y += dir.y;
            i++;
        }
        return (i > 0)
    }
}
export class MoveAll extends AMagmaxCard
{
    public execute(position:Vector2, orientation?:MagmaxTypes.Orientation):boolean
    {
        position;
        if (!orientation)
            return (false);
        return (this.board.moveAll(this.player.ID, orientation));
    }
}
export class ShootWater extends AMagmaxCard
{
    public execute(position:Vector2):boolean
    {
        const pawn = this.board.get(position)?.pawn;
        let dir:Vector2;
        let tile:MagmaxTypes.Tile|undefined;

        if (!pawn)
            return (false);
        dir = MagmaxBoard.transformOrientationToDirection(pawn.orientation);
        while (!this.board.isOutsideRange(position.x, position.y)) {
            position.x += dir.x;
            position.y += dir.y;
            tile = this.board.get(position);
            if (tile?.hasRock) {
                tile.hasRock = false;
                break;
            }
            if (tile?.pawn) {
                if (!tile.pawn.isProtected) {
                    tile.pawn = undefined;
                }
                else
                    tile.pawn.isProtected = false;
                break;
            }
        }
        return (true);
    }
}
export class ShootLaser extends AMagmaxCard
{
    public execute(position:Vector2):boolean
    {
        const pawn = this.board.get(position)?.pawn;
        let dir:Vector2;
        let tile:MagmaxTypes.Tile|undefined;
        let hasBeenRefracted = false;

        if (!pawn)
            return (false);
        dir = MagmaxBoard.transformOrientationToDirection(pawn.orientation);
        while (!this.board.isOutsideRange(position.x, position.y)) {
            position.x += dir.x;
            position.y += dir.y;
            tile = this.board.get(position);
            if (tile?.hasRock) {
                tile.hasRock = false;
                break;
            }
            if (tile?.pawn) {
                if (tile.pawn.isProtected && !hasBeenRefracted) {
                    dir = MagmaxBoard.transformOrientationToDirection(tile.pawn.orientation);
                    hasBeenRefracted = true;
                }
                if (!tile.pawn.isProtected) {
                    tile.pawn = undefined;
                    break;
                }
            }
        }
        return (true);
    }
}
export class ShootLight extends AMagmaxCard
{
    public execute(position:Vector2):boolean
    {
        const pawn = this.board.get(position)?.pawn;
        let dir:Vector2;
        let tile:MagmaxTypes.Tile|undefined;
        let hasBeenRefracted = false;

        if (!pawn)
            return (false);
        dir = MagmaxBoard.transformOrientationToDirection(pawn.orientation);
        while (!this.board.isOutsideRange(position.x, position.y)) {
            position.x += dir.x;
            position.y += dir.y;
            tile = this.board.get(position);
            if (tile?.hasRock)
                tile.hasRock = false;
            if (tile?.pawn) {
                if (tile.pawn.isProtected && !hasBeenRefracted) {
                    dir.x *= -1;
                    dir.y *= -1;
                    hasBeenRefracted = true;
                }
                if (!tile.pawn.isProtected)
                    tile.pawn = undefined;
            }
        }
        return (true);
    }
}
export class PlaceBomb extends AMagmaxCard
{
    public execute(position:Vector2):boolean
    {
        const tile = this.board.get(position);

        if (!tile || !tile.pawn)
            return (false);
        if (tile.hasBomb)
            return (false);
        tile.hasBomb = true;
        return (true);
    }
}
export class PlaceRock extends AMagmaxCard
{
    public execute(position:Vector2):boolean
    {
        const tile = this.board.get(position);
        let dir:Vector2;
        let nxtPos:Vector2;
        let destination;

        if (!tile || !tile.pawn)
            return (false);
        dir = MagmaxBoard.transformOrientationToDirection(tile.pawn!.orientation);
        nxtPos = {x: position.x + dir.x, y: position.y + dir.y};
        destination = this.board.get(nxtPos);
        if (!destination || destination.hasBomb || destination.hasRock)
            return (false);
        if (destination.pawn)
            destination.pawn = undefined;
        destination.hasRock = true;
        return (true);
    }
}
export class Protect extends AMagmaxCard
{
    public execute(position:Vector2):boolean
    {
        const pawn = this.board.get(position)?.pawn;
        
        if (!pawn)
            return (false);
        pawn.isProtected = true;
        return (true);
    }
}
export class ShowBombs extends AMagmaxCard
{
    public execute(position:Vector2):boolean
    {
        const pawn = this.board.get(position)?.pawn;

        if (!pawn)
            return (false);
        pawn.seeBombs = true;
        return (true);
    }
}