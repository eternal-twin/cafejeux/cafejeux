import { PlayerId } from "etwin-socket-server";
import { LinearMatrix, Vector2 } from "./../../Utils";
import { MagmaxTypes } from "./MagmaxTypes";

export class MagmaxBoard extends LinearMatrix<MagmaxTypes.Tile>
{
    private static readonly PADDING = 1;
    
    constructor(size:number)
    {
        super(size);
        this.generate();
        this.generateRocks();
    }

    public static transformOrientationToDirection(orientation:MagmaxTypes.Orientation):Vector2
    {
        const output = {x: 0, y: 0};

        output.x += orientation === MagmaxTypes.Orientation.WEST ? - 1 : 0;
        output.x += orientation === MagmaxTypes.Orientation.EAST ? 1 : 0;
        output.y += orientation === MagmaxTypes.Orientation.SOUTH ? 1 : 0;
        output.y += orientation === MagmaxTypes.Orientation.NORTH ? - 1 : 0;
        return (output);
    }

    private generate()
    {
        for (let x = this.getWidth() - 1; x >= 0; x--) {
            for (let y = this.getHeight() - 1; y >= 0; y--) {
                this.set(x, y, {});
            }
        }
    }
    private generateRocks()
    {
        const WIDTH = this.getWidth() - MagmaxBoard.PADDING * 2;
        const MIDDLE = ~~(this.getWidth() * 0.5);
        let y;

        this.set(MIDDLE, MIDDLE, {hasRock: true});
        for (let i = WIDTH; i >= MagmaxBoard.PADDING; i--) {
            if (i === MIDDLE)
                continue;
            y = MagmaxBoard.PADDING + ~~(Math.random() * WIDTH);
            this.set(i, y, {hasRock: true});
        }
    }
    private movePawn(from:Vector2, to:Vector2)
    {
        const start = this.get(from);
        const destination = this.get(to);

        if (!start || !destination)
            return;
        destination.pawn = start.pawn;
        start.pawn = undefined;
    }

    public move(pos:Vector2, orientation:MagmaxTypes.Orientation):boolean
    {
        const pawn = this.get(pos)?.pawn;
        const dir = MagmaxBoard.transformOrientationToDirection(orientation);
        const nxtPos = {x: pos.x + dir.x, y: pos.y + dir.y};
        const destination = this.get(nxtPos);

        if (!pawn)
            return (false);
        if (destination?.hasRock)
            return (false);
        //temporary: The player may be pushed twice in case of moveAll.
        if (destination?.pawn && !this.move(nxtPos, orientation))
            return (false);
        if (this.isOutsideRange(nxtPos.x, nxtPos.y) || destination?.hasBomb) {
            this.get(pos)!.pawn = undefined;
        }
        if (destination?.hasBomb) {
            this.set(nxtPos.x, nxtPos.y, {});
        }
        this.movePawn(pos, nxtPos);
        pawn.orientation = orientation;
        return (true);
    }
    public moveAll(id:PlayerId, orientation:MagmaxTypes.Orientation):boolean
    {
        let output = false;
        const pawnPositions:Vector2[] = [];
        
        this.forEach((tile, x, y) => {
            if (tile?.pawn?.owner === id) {
                pawnPositions.push({x, y});
            }
        });
        for (let i = pawnPositions.length - 1; i >= 0; i--) {
            output = this.move(pawnPositions[i], orientation) || output;
        }
        return (output);
    }
    public getData(hiddenData:boolean):LinearMatrix<MagmaxTypes.Tile>
    {
        const output = new LinearMatrix<MagmaxTypes.Tile>(this.getWidth());

        this.forEach((data, x, y) => {
            if (!data || (!data.hasRock && !data.hasBomb && !data.pawn))
                return;
            output.set(x, y, {
                hasRock: data.hasRock,
                hasBomb: !hiddenData && data.hasBomb,
                pawn: data.pawn ? data.pawn.getData(hiddenData) : undefined
            })
        });
        return (output);
    }
    public setRock(x:number, y:number, tile:MagmaxTypes.Tile)
    {
        const t = this.get(x, y);

        if(!t)
            return;
        t.hasBomb = tile.hasBomb !== undefined ? tile.hasBomb : t.hasBomb;
        t.hasRock = tile.hasRock !== undefined ? tile.hasRock : t.hasRock;
        t.pawn = tile.pawn !== undefined ? tile.pawn : t.pawn;
    }
}