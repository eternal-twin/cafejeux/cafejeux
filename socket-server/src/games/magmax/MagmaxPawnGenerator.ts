import { PlayerId } from "etwin-socket-server";
import { MagmaxGame } from "./MagmaxGame";
import { MagmaxPawn } from "./MagmaxPawn";
import { MagmaxBoard } from "./MagmaxBoard";
import { MagmaxTypes } from "./MagmaxTypes";

export class MagmaxPawnGenerator
{
    private isBottom = false;

    /**
     * Generate 3 pawns owned by a player, and place them on the board passed as parameter.
     * At every call, the function changes the side of the generated pawns.
     *
     * @params board:MagmaxBoard = The board on which pawns must be placed.
     * @params owner:PlayerId = The owner of the generated pawns.
     * @returns An array of MagmaxPawn.
     */
    public generate(board:MagmaxBoard, owner:PlayerId):MagmaxPawn[]
    {
        const MID = ~~(MagmaxGame.SIZE / 2);
        const DIR = this.isBottom ? 1 : -1;
        const DEFAULT = this.isBottom ? MagmaxGame.SIZE - 2 : 1;
        const ORIENTATION = this.isBottom ? MagmaxTypes.Orientation.NORTH : MagmaxTypes.Orientation.SOUTH;
        const output = [
            new MagmaxPawn(owner, ORIENTATION),
            new MagmaxPawn(owner, ORIENTATION),
            new MagmaxPawn(owner, ORIENTATION)
        ];

        board.set(MID, DEFAULT, {pawn: output[0]});
        board.set(MID - 1, DEFAULT + DIR, {pawn: output[1]});
        board.set(MID + 1, DEFAULT + DIR, {pawn: output[2]});
        this.isBottom = !this.isBottom;
        return (output);
    }
}