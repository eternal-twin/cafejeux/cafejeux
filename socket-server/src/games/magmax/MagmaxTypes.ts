import { MagmaxPawn } from "./MagmaxPawn";

export module MagmaxTypes
{
    export enum Orientation
    {
        NORTH = "north",
        WEST = "west",
        EAST = "east",
        SOUTH = "south"
    };
    export interface Tile
    {
        hasBomb?:boolean;
        hasRock?:boolean;
        pawn?:MagmaxPawn;
    };
}