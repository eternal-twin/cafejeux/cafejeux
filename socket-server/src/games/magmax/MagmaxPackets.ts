import { IReceivedPacket, ISendPacket } from "etwin-socket-server";
import { MagmaxTypes } from "./MagmaxTypes";
import { LinearMatrix, Vector2 } from "./../../Utils";

export module MagmaxPackets
{
    export interface Start extends ISendPacket
    {
        color:string;
        board:LinearMatrix<MagmaxTypes.Tile>;
        deck:string[];
    }
    export interface Update extends ISendPacket
    {
        board?:LinearMatrix<MagmaxTypes.Tile>;
        deck:string[];
    }
    export interface Play extends IReceivedPacket
    {
        card:string;
        position:Vector2;
        orientation?:MagmaxTypes.Orientation;
    }
    export interface End extends ISendPacket
    {
        hasWon:boolean;
    }
}