import { ACafejeuxGame } from "./../../ACafejeuxGame";
import { MagmaxBoard } from "./MagmaxBoard";
import { MagmaxCardBuilder } from "./MagmaxCardBuilder";
import { MagmaxPackets } from "./MagmaxPackets";
import { MagmaxPawnGenerator } from "./MagmaxPawnGenerator";
import { MagmaxPlayer } from "./MagmaxPlayer";

export class MagmaxGame extends ACafejeuxGame<MagmaxPlayer>
{
    public static readonly SIZE = 7;
    public static readonly DECK_SIZE = 6;
    public static readonly CARDS_BY_TURN = 3;
    private readonly colors = ["red", "blue"];
    private readonly pawnGenerator = new MagmaxPawnGenerator();
    private readonly board = new MagmaxBoard(MagmaxGame.SIZE);
    private readonly cardBuilder = new MagmaxCardBuilder(this.board);

    constructor(opts:any)
    {
        super(opts);
        this.registerReceiveEvent("play", this.playTurn);
    }

    private playTurn = (data:MagmaxPackets.Play, emitter:MagmaxPlayer) =>
    {
        if (!this.checkTurnIntegrity(emitter.ID))
            return;
        if (!data.card || !data.position) {
            if (emitter.deck.length >= MagmaxGame.DECK_SIZE)
                return;
            this.cardBuilder.player = emitter;
            for (let i = 0; i < MagmaxGame.CARDS_BY_TURN; i++) {
                emitter.deck.push(this.cardBuilder.getCard());
                if (emitter.deck.length >= MagmaxGame.DECK_SIZE)
                    break;
            }
            this.sendUpdate();
            this.changeTurn();
            return;
        }
        this.isMoveAllowed(data, emitter);
        this.sendUpdate();
        this.apply((p) => !p.isAlive(this.board) ? this.stop() : undefined);
    }
    private isMoveAllowed(data:MagmaxPackets.Play, emitter:MagmaxPlayer):boolean
    {
        const card = emitter.pickCard(data.card);

        if (!card) {
            return (false);
        }
        if (!card.execute(data.position, data.orientation)) {
            emitter.deck.push(card);
            return (false);
        }
        return (true);
    }
    private sendUpdate()
    {
        this.apply((p) => {
            p.send<MagmaxPackets.Update>("update", {
                board: this.board.getData(!p.canSeeBombs),
                deck: p.getDeck()
            });
        });
    }
    
    protected getDataOnReconnection = (p:MagmaxPlayer) =>
    {
        return (<MagmaxPackets.Start>{
            color: p.color,
            board: this.board.getData(!p.canSeeBombs),
            deck: p.getDeck()
        });
    }

    public run()
    {
        this.apply((p) => {
            this.cardBuilder.player = p;
            p.pawns = this.pawnGenerator.generate(this.board, p.ID);
            p.deck = this.cardBuilder.generateHand();
            p.color = this.colors.pop()!
            p.send("start", this.getDataOnReconnection(p))
        });
    }
    public close()
    {
        this.apply((p) => {
            p.send<MagmaxPackets.End>("end", {
                hasWon: p.isAlive(this.board)
            });
        });
    } 
}