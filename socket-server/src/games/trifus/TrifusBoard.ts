import { TrifusTypes } from "./TrifusTypes";
import { LinearMatrix, Vector2 } from "./../../Utils";

export class TrifusBoard extends LinearMatrix<TrifusTypes.Card>
{
	private static readonly NB_CARDS_PER_ELEMENT = 8;
	private static readonly NB_DEFAULT_CARDS = 4;
	private readonly deck:Array<TrifusTypes.Card> = [];

	constructor(width:number, height:number)
	{
		super(width, height);

		this.generateCards();
		this.generateBoard();
	}

	private generateCards()
	{
		let card:TrifusTypes.Card;

		for (let i = TrifusTypes.Element.COUNT - 1; i > TrifusTypes.Element.START; i--) {
			for (let j = TrifusBoard.NB_CARDS_PER_ELEMENT - 1; j >= 0; j--) {
				card = {
					strength: j + 1,
					element: i,
					type: TrifusTypes.toString(i)!,
					owner: undefined
				};
				this.deck.push(card);
			}
		}
		
	}
	private generateBoard()
	{
		let index:number;
		let pos:Vector2;
		let card:TrifusTypes.Card;
		
		for (let i = TrifusBoard.NB_DEFAULT_CARDS - 1; i >= 0; i--) {
			index = ~~(Math.random() * this.deck.length);
			pos = this.getRandomFreeTile();
			card = this.deck.splice(index, 1)[0];
			this.set(pos.x, pos.y, card);
		}
	}
	private getRandomFreeTile():Vector2
	{
		let pos;

		do {
			pos = {
				x: ~~(Math.random() * this.getWidth()),
				y: ~~(Math.random() * this.getHeight())
			}
		} while (this.get(pos) != undefined);
		return (pos);
	}

	public generateDecks(nbDecks:number):Array<TrifusTypes.Card[]>
	{
		let output:Array<TrifusTypes.Card[]> = [];
		let cur = 0;
		let index:number;

		for (let i = nbDecks - 1; i >= 0; i--)
			output.push([]);
		while (this.deck.length > 0) {
			index = ~~(Math.random() * this.deck.length);
			output[cur].push(this.deck.splice(index, 1)[0]);
			cur = (cur + 1) % nbDecks;
		}
		return (output);
	}
	public computeScore(color:TrifusTypes.Color):number
	{
		let output = 0;
		let card:TrifusTypes.Card|undefined;

		for (let x = this.getWidth() - 1; x >= 0; x--) {
			for (let y = this.getHeight() - 1; y >= 0; y--) {
				card = this.get(x, y);
				if (!card)
					continue;
				if (card.owner === color)
					output += card.strength;
			}
		}
		return (output);
	}
}