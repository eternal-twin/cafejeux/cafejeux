import { ACafejeuxGame } from "./../../ACafejeuxGame";
import { TrifusBoard } from "./TrifusBoard";
import { TrifusPackets } from "./TrifusPackets";
import { TrifusPlayer } from "./TrifusPlayer";
import { TrifusTypes } from "./TrifusTypes";
import { TrifusWeaknessManager } from "./TrifusWeaknessManager";
import { Vector2 } from "./../../Utils";

export class TrifusGame extends ACafejeuxGame<TrifusPlayer>
{
	private static readonly DIR:Vector2[] = [
		{x: -1, y: 0},
		{x: 1, y: 0},
		{x: 0, y: -1},
		{x: 0, y: 1}
	];
	private static readonly WIDTH = 6;
	private static readonly HEIGHT = 4;
	private readonly board = new TrifusBoard(TrifusGame.WIDTH, TrifusGame.HEIGHT);
	private readonly weaknessManager = new TrifusWeaknessManager();
	private readonly colors = ["blue", "red"];

    constructor(opts:any)
    {
        super(opts);
		this.registerReceiveEvent("play", this.playTurn);
	}
	
	private playTurn = (data:TrifusPackets.Play, emitter:TrifusPlayer) =>
	{
		const POS = data.position;
		let card:TrifusTypes.Card|undefined;
		let output:TrifusTypes.PlacedCard[];

		if (!POS || !this.checkTurnIntegrity(emitter.ID) || !this.isMoveValid(POS))
			return;
		card = emitter.pickCard(data.card);
		if (!card)
			return;
		output = this.placeCard(POS, card);
		this.broadcast<TrifusPackets.Update>("update", {
			cards: output
		});
		if (this.isGameEnded()) {
			this.stop();
			return;
		}
		this.changeTurn();
	}
	/**
	 * A move is considered as valid if:
	 * - The position is not outside of the board.
	 * - The position is not over another card.
	 *
	 * @params pos:Vector2 = The position of the card to place.
	 * @returns True if the move is valid, false otherwise.
	 */
	private isMoveValid(pos:Vector2):boolean
	{
		if (pos.x < 0 || pos.x >= this.board.getWidth()
			|| pos.y < 0 || pos.y >= this.board.getHeight())
			return (false);
		if (this.board.get(pos.x, pos.y) != undefined)
			return (false);
		return (true);
	}
	/**
	 * Place the card on the board.
	 * It also returns an array containing all cards which were affected by the move.
	 * If the move is wrong, an empty array will by returned.
	 *
	 * @params pos:Vector2 = The position where the card should be placed.
	 * @params card:TrifusTypes.Card = The data to be written
	 * @returns An array containing all the cards that were modified by the move.
	 */
	private placeCard(pos:Vector2, card:TrifusTypes.Card):TrifusTypes.PlacedCard[]
	{
		const output:TrifusTypes.PlacedCard[] = [
			{position: pos, card: card}
		];
		let cur:TrifusTypes.Card|undefined;
		let npos:Vector2;

		this.board.set(pos.x, pos.y, card);
		for (let i = TrifusGame.DIR.length - 1; i >= 0; i--) {
			npos = {
				x: pos.x + TrifusGame.DIR[i].x,
				y: pos.y + TrifusGame.DIR[i].y
			};
			cur = this.board.get(npos);
			if (!cur)
				continue;
			if (this.weaknessManager.isCardWeaker(cur, card)) {
				cur.owner = card.owner;
				output.push({position: npos, card: cur});
			}
		}
		return (output);
	}
	private isGameEnded():boolean
	{
		const playersWithNotEmptyDeck = this.filter((p) => {
			return (p.deck.length > 0);
		});

		return (playersWithNotEmptyDeck.length === 0);
	}

	protected getDataOnReconnection = (p:TrifusPlayer) =>
	{
		return (<TrifusPackets.Start>{
			board: this.board,
			color: p.color,
			deck: p.deck
		});
	}
	
	public run()
	{
		const decks = this.board.generateDecks(ACafejeuxGame.ROOM_CAPACITY);

		this.apply((p) => {
			p.deck = decks.shift()!;
			p.color = this.colors.shift()!;
			p.send<TrifusPackets.Start>("start", this.getDataOnReconnection(p));
		});
	}
	public close()
	{
        let highestScore = 0;

        this.apply((p) => {
        	p.score = this.board.computeScore(p.color);
            if (p.score > highestScore)
                highestScore = p.score;
        });
		this.apply((p) => {
			p.send<TrifusPackets.End>("end", {
				score: p.score,
				hasWon: p.score === highestScore
			});
		});
	}
}