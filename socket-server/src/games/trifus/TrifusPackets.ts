import { IReceivedPacket, ISendPacket } from "etwin-socket-server";
import { TrifusBoard } from "./TrifusBoard";
import { TrifusTypes } from "./TrifusTypes";
import { Vector2 } from "./../../Utils";

export namespace TrifusPackets
{
	export interface Start extends ISendPacket
	{
		board:TrifusBoard;
		deck:TrifusTypes.Card[];
		color:TrifusTypes.Color;
	}
	export interface Update extends ISendPacket
	{
		cards:TrifusTypes.PlacedCard[];
	}
	export interface End extends ISendPacket
	{
		score:number;
		hasWon:boolean;
	}
	export interface Play extends IReceivedPacket
	{
		position:Vector2;
		card:TrifusTypes.Card;
	}
}