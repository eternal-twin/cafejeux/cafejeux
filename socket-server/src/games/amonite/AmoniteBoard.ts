import { PlayerId } from "etwin-socket-server";
import { Vector2 } from "./../../Utils";

export type AmoniteBoardDataType = Array<Array<PlayerId|undefined>>;
export class AmoniteBoard
{
    public static readonly MIN_HEIGHT = 5;
    public static readonly MAX_HEIGHT = 9;
    private static readonly GROUP_PAWN_SIZE = 3;
    private readonly data:AmoniteBoardDataType = [];
    private readonly ids:Array<PlayerId> = [];

    constructor()
    {
        this.generate();
    }

    private generate()
    {
        const GAP = (AmoniteBoard.MAX_HEIGHT - AmoniteBoard.MIN_HEIGHT);
        
        for (let i = -GAP; i <= GAP; i++) {
            if (i === 0) {
                this.data.push(new Array(AmoniteBoard.MAX_HEIGHT));
                continue;
            }
            this.data.push(new Array((GAP - Math.abs(i) + AmoniteBoard.MIN_HEIGHT)));
        }
    }
    private isOutside(p:Vector2):boolean
    {
        return (p.x < 0 || p.x >= this.data.length || p.y < 0 || p.y >= this.data[p.x].length);
    }
    
    /**
     * Returns all positions of every pawns owned by a player.
     * If id is not set, it will returns all positions of free tile.
     *
     * @params id:PlayerId|undefined = The id of the player.
     * @returns An array containing all the positions where there is a pawn.
     */
    private getPawns(id:PlayerId|undefined):Vector2[]
    {
        let output:Vector2[] = [];
        
        for (let x = this.data.length - 1; x >= 0; x--) {
            for (let y = this.data[x].length - 1; y >= 0; y--) {
                if (this.data[x][y] === id)
                    output.push({x, y});
            }
        }
        return (output);
    }
    private getPositionsAroundTile(pos:Vector2):Vector2[]
    {
        const output:Vector2[] = [];
        const dir = [-1, 1];
        let padding:number;

        if (this.isOutside(pos))
            return ([]);
        if (pos.y - 1 >= 0)
            output.push({x: pos.x, y: pos.y - 1});
        if (pos.y < this.data[pos.x].length)
            output.push({x: pos.x, y: pos.y + 1});
        for (let i = dir.length - 1; i >= 0; i--) {
            if (pos.x + dir[i] < 0 || pos.x + dir[i] >= this.data.length)
                continue;
            padding = this.data[pos.x].length < this.data[pos.x + dir[i]].length ? 0 : -1;
            output.push({x: pos.x + dir[i], y: pos.y + padding});
            output.push({x: pos.x + dir[i], y: pos.y + 1 + padding});
        }
        return (output);
    }
    private arePositionsNextToEachOther(p1:Vector2, p2:Vector2):boolean
    {
        const positionsAround = this.getPositionsAroundTile(p1);
        
        for (let i = positionsAround.length - 1; i >= 0; i--) {
            if (p2.x === positionsAround[i].x && p2.y === positionsAround[i].y)
                return (true);
        }
        return (false);
    }
    /**
     * Check if there is a valid path between the position and the target.
     * The path is made by jumping over the intermediates points.
     *
     * @params pos:Vector2 = The starting point.
     * @params target:Vector2 = The ending point.
     * @params intermediates:Vector2[] = The points to jump over to reach the target.
     * @returns Returns true if it is possible to draw a path between pos and target
     */
    private canReachTarget(pos:Vector2, target:Vector2, intermediates:Vector2[]):boolean
    {
        const cpy = intermediates.slice();
        let nextIntermediate:Vector2;
        let pa:Vector2[];

        if (cpy.length === 0)
            return (false);
        for (let i = cpy.length - 1; i >= 0; i--) {
            if (!this.arePositionsNextToEachOther(pos, cpy[i]))
                continue;
            nextIntermediate = cpy.splice(i, 1)[0];
            if (this.arePositionsNextToEachOther(target, nextIntermediate))
                return (true);
            pa = this.getPositionsAroundTile(nextIntermediate);
            for (let j = pa.length - 1; j >= 0; j--) {
                if (this.data[cpy[0].x]?.[cpy[0].y] === this.data[pa[j].x]?.[pa[j].y])
                    continue;
                if (this.arePositionsNextToEachOther(pos, pa[j]) || this.isOutside(pa[j]))
                    continue;
                if (pa[j].x === target.x && pa[j].y === target.y)
                    return (true);
                if (this.canReachTarget(pa[j], target, cpy))
                    return (true);
            }
        }
        return (false);
    }
    /**
     * Count tiles which have a link with the current tile.
     * A link is a path composed of tiles with the same type as the tile of the current position.
     *
     * @params current:Vector2 = The starting point.
     * @params tiles:Vector2[] = All the positions that will be checked on the board.
     * @returns The numlber of tiles linked to the current tile.
     */
    private countLinkedTiles(current:Vector2, tiles:Vector2[]):number
    {
        let positionsAround = this.getPositionsAroundTile(current);
        let output = 0;

        positionsAround = positionsAround.filter((pos) => {
            for (let i = tiles.length - 1; i >= 0; i--) {
                if (tiles[i].x === pos.x && tiles[i].y === pos.y) {
                    tiles.splice(i, 1);
                    return (true);
                }
            }
            return (false);
        });
        output = positionsAround.length;
        for (let i = positionsAround.length - 1; i >= 0; i--) {
            output += this.countLinkedTiles(positionsAround[i], tiles);
        }
        return (output);
    }

    /**
     * Generate pawns by groups of 3 in every corner of the haxagonal map.
     * Every generated pawn has an empty space or an adversary pawn as neighbour.
     */
    public generatePawns()
    {
        const LEN = this.data.length;
        let yLen:number;
        let absX:number;
        let dir:number;
        let cur = 0;
        let spacing = 2;

        for (let x = -(LEN - 1 - 1); x < LEN; x++) {
            absX = x < 0 ? -x : x;
            yLen = this.data[absX].length - 1;
            dir = Math.sign(x === 0 ? -1 : x);
            dir *= (absX === 0 || absX >= LEN - 1) ? -1 : 1;
            for (let y = x <= 0 ? 0 : yLen; y >= 0 && y <= yLen; y += dir) {
                spacing = (spacing + 1) % (AmoniteBoard.GROUP_PAWN_SIZE + 1);
                if (spacing === 0) {
                    continue;
                }
                this.data[absX][y] = this.ids[cur];
                cur = (cur + 1) % this.ids.length;
            }
        }
    }
    public isPawnOwnedByPlayer(pos:Vector2, id:PlayerId):boolean
    {
        return (this.data[pos.x]?.[pos.y] === id);
    }
    public isMoveValid(pos:Vector2, target:Vector2):boolean
    {
        const id:PlayerId|undefined = this.data[pos.x]?.[pos.y];
        const pawns = this.getPawns(id).filter((p) => p.x !== pos.x || p.y !== pos.y);

        if (!id)
            return (false);
        if (pos.x - target.x === 0 && pos.y - target.y === 0)
            return (false);
        if (this.arePositionsNextToEachOther(pos, target)) {
            if (this.data[target.x]?.[target.y])
                return (false);
            return (true);
        }
        return (this.canReachTarget(pos, target, pawns));
    }
    /**
     * Returns true if all pawns are a player are next to each other.
     *
     * @params id:PlayerId = The player pawn's id to check.
     * @returns True if all pawns are next to each other, false otherwise.
     */
    public areAllPawnsLinked(id:PlayerId):boolean
    {
        const pawns = this.getPawns(id);
        const first = pawns.shift();
        const LEN = pawns.length;

        if (!first)
            return (false);
        return (this.countLinkedTiles(first, pawns) === LEN);
    }
    public movePawn(pos:Vector2, target:Vector2)
    {
        this.data[target.x][target.y] = this.data[pos.x][pos.y];
        this.data[pos.x][pos.y] = undefined;
    }
    public registerPlayerId(id:PlayerId)
    {
        this.ids.push(id);
    }
    public getData():AmoniteBoardDataType
    {
        return (this.data);
    }
}