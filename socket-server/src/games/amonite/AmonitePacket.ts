import { PlayerId, ISendPacket, IReceivedPacket } from "etwin-socket-server";
import { AmoniteBoardDataType } from "./AmoniteBoard";
import { Vector2 } from "./../../Utils";

export namespace AmonitePackets
{
    export interface Start extends ISendPacket
    {
        maxHeight:number;
        minHeight:number;
        board:AmoniteBoardDataType;
        color:string;
    }
    export interface Update extends ISendPacket
    {
        position:Vector2;
        target:Vector2;
        id:PlayerId;
    }
    export interface End extends ISendPacket
    {
        hasWon:boolean;
    }
    export interface Play extends IReceivedPacket
    {
        position:Vector2;
        target:Vector2;
    }
}