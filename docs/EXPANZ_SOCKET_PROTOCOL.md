# Expanz Socket Protocol

This file describe every event you can send/receive from client-side.

## Packet sent from server to client-side

EVERY packet of this side also has a senderId attribute, which let you identify your users.

### Start event

Event name: **start**

Description:\
This event is triggered when the game starts.

Content:
* board: LinearMatrix<ExpanzTypes.Color>

### Update board

Event name: **update**

Description:\
This event is triggered when a player's move is valid.\
It returns the id of the player who played, the taken color, and the whole territory of the player (not only the newly annexed tiles).

Content:
* playerId: PlayerId
* color: ExpanzTypes.Color
* territory: Vector2[]

### Next turn event

Event name: **change_turn**

Description:\
This event is sent when the next turn occurs, always after the **update** event.\
The difference with update is that this event can be send multiple in a row.
If it is sent multiple times, it means a player can't play.\
You can detect easily if a turn has been skipped: if a **change_turn** event has been fired without an **update** event before, it means the turn has been skipped.\
You'll also have access to the current player id and its remaining time before time up.

Content:
* current: PlayerId
* remainingTime: number

### Game ended event

Event name: **end**

Description:\
This event is sent when the game is ended.\
The game is considerated as ended when both players can't play.\
It contains an array of ExpanzTypes.Summary objects.

Content:
* summary: Array<Summary>

## Packet sent from client to server-side

### Send move event

Event name: **play**

Description:\
Send the position where the player wish to play.\
*Nothing will happen in the following cases:*
* *It is not the player's turn.*
* *The tile's color at the choosen position is already taken.*
* *The choosen position does not create a link with the player's territory.*

Content:
* position: {x:number, y:number}

## Objects

### PlayerId

PlayerId is a simple string.

### ExpanzTypes.Color

ExpanzTypes.Color is a simple number between 0 and 4 included.

### LinearMatrix<ExpanzTypes.Color>

A linear matrix is a complex object, it is a multidimentional array optimized to fit in a single-dimention array.\
It is possible to reconstitute a bidimentional array by splitting the array in subarray of length "*width*", or access a tile at (x, y) with the following operation: ```y * width + x```.

Content:
* data: Array<ExpanzTypes.Color>
* width: number
* height: number
