# Quatcinelle Socket Protocol

This file describe every event you can send/receive from client-side.

## Packet sent from server to client-side

EVERY packet of this side also has a senderId attribute, which let you identify your users.

### Start event

Event name: **start**

Description:\
This event is triggered when the game starts.
It sends the board, the deck and the color of the player.

Content:
* board: LinearMatrix<TrifusTypes.Card>
* deck: TrifusTypes.Card[]
* color: string

### Update board

Event name: **update**

Description:\
This event is triggered when a player's move is valid.\
It returns an array containing all the cards modified by the move.

Content:
* cards: TrifusTypes.PlacedCards[]

### Next turn event

Event name: **change_turn**

Description:\
This event is sent when the next turn occurs, always after the **update** event.

Content:
* current: PlayerId
* remainingTime: number

### Game ended event

Event name: **end**

Description:\
This event is sent when the game is ended.\
The game is considerated as ended when all cards were played.\
It returns a boolean indicates whether or not the player has won.

Content:
* hasWon: boolean

## Packet sent from client to server-side

### Send move event

Event name: **play**

Description:\
Send a position and a card where the player wish to play.\
*Nothing will happen in the following cases:*
* *It is not the player's turn.*
* *The card is not in the player's hand.*
* *The card is over other cards.*
* *The card is outside of the board.*

Content:
* position: {x:number, y:number}
* card: {type:string, strength:number}

## Objects

### PlayerId

PlayerId is a simple string.

### LinearMatrix<TrifusTypes.Card>

A linear matrix is a complex object, it is a multidimentional array optimized to fit in a single-dimention array.\
It is possible to reconstitute a bidimentional array by splitting the array in subarray of length "*width*", or access a tile at (x, y) with the following operation: ```y * width + x```.

Content:
* data: Array<TrifusTypes.Card>
* width: number
* height: number

### TrifusTypes.Card

An object containing its owner (the color of the player or undefined if there is no owner), its strength and its type.

Content:
* strength: number
* element: number
* type: string
* owner: string|undefined

### TrifusTypes.PlacedCard

An object containing a card and the position where should be placed this card.

Content:
* position: {x:number, y:number}
* card: TrifusTypes.Card