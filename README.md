# Cafejeux

## Official
 * URL: (http://cafejeux.com/)
 * Creation date: ?
 * Main MT administrators: ?
 * SWF compiler: ?

## Eternal Twin

* Authors: @RahlDarkol & @AlexandreVolts
* Repository (https://gitlab.com/eternal-twin/cafejeux.git)

## Run the project


### Install dependencies

```yarn install```

*This project requires [Node.js](https://nodejs.org/en/download/) and [Yarn](https://classic.yarnpkg.com/en/docs/install#windows-stable)*

### Build the project

```yarn build```

*You must install Typescript Compiler globally, so try this command if it didn't work:*
```yarn add global typescript```

### Launch the socket server

```yarn run start-ss```

### Launch the API server

### Launch the front-end

```yarn run start-app```