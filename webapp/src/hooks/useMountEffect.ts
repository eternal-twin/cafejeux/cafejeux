import React from "react";

export const useMountEffect = (callback:React.EffectCallback) => React.useEffect(callback, []);