import React from "react";
import { UserContext } from "./../contexts/UserContext";

export const useSounds = (names:string[]) =>
{
    const context = React.useContext(UserContext);
    const [audios] = React.useState(names.map((name) => ({
        name: name,
        url: `./sounds/${name}.mp3`, 
        source: new Audio(`${process.env.PUBLIC_URL}/sounds/${name}.mp3`)
    })));
    
    const playSound = (name:string) => {
        if (!context.soundOn)
            return;
        audios.forEach((audio) => {
            if (audio.name === name)
                audio.source.play();
        });
    }

    return ([playSound]);
};