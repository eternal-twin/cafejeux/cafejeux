import React from "react";
import { useParams } from "react-router-dom";
import SocketIOClient from "socket.io-client";
import { useMountEffect } from "./hooks/useMountEffect";
import Chat from "./components/Chat";
import GameContainer from "./components/GameContainer";
import GameContextProvider from "./contexts/GameContext";
import StylesDictionary from "./generics/StylesDictionary";

const URL = process.env.REACT_APP_SOCKET_URL || "http://localhost:8888";

interface GameInterfaceProps
{
    
}

export default function GameInterface(props:GameInterfaceProps)
{
    const { game } = useParams<{game:string}>();
    const [ socket, setSocket ] = React.useState() as [SocketIOClient.Socket, Function];

    useMountEffect(() => {
        const socket = SocketIOClient(URL, { withCredentials: true } as any);

        socket.emit("room", { room: game, opts: {public: true}});
        setSocket(socket);
        return (() => {
            socket.disconnect();
        });
    });
    
    if (!socket) {
        return <div>Loading...</div>
    }
    return (
        <GameContextProvider>
            <div style={styles.container}>
                <div style={{flex: 1}}>
                    <Chat
                        socket={socket}
                    />
                </div>
                <div style={{marginLeft: 10}}>
                    <GameContainer
                        name={game}
                        socket={socket}
                    />
                </div>
            </div>
        </GameContextProvider>
    );
}

const styles:StylesDictionary = {
    container: {
        padding: 10,
        display: "flex",
        width: "100%",
        height: 300
    }
};