import React from "react";
import "./PlayerInfos.css";
import BackgroundImageContainer from "./../generics/BackgroundImageContainer";
import Progress from "./../generics/Progress";
import StylesDictionary from "./../generics/StylesDictionary";

interface PlayerInfosProps
{
    username:string;
    level:number;
    xp:number;
    remainingTime:number;
    maxXp?:number;
    reverse?:boolean;
    active?:boolean;
}

export default function PlayerInfos(props:PlayerInfosProps)
{
    const [time, setTime] = React.useState(props.remainingTime);

    React.useEffect(() => {
        const interval = setInterval(() => {
            if (props.active)
                setTime(time - 1);
        }, 1000);

        return (() => {
            clearInterval(interval);
        });
    }, [time, setTime, props.active]);
    
    React.useEffect(() => {
        setTime(props.remainingTime);
    }, [setTime, props.remainingTime]);
    
    const displayTextInfos = () => {
        if (props.reverse) {
            return (
                <p style={{margin: "0 4px", textAlign: "right"}}>
                    ({format(time)}) {props.username}
                </p>
            );
        }
        else {
            return (
                <p style={{margin: "0 4px"}}>
                    {props.username} ({format(time)})
                </p>
            );
        }
    }
    
    return (
        <div style={{...styles.container, ...{
                flexDirection: props.reverse ? "row-reverse" : "row"
            }}}
            className={props.active ? "active" : ""}
        >
            <BackgroundImageContainer
                url="player_background.png"
                style={styles.avatarBox}
            >

            </BackgroundImageContainer>
            <div style={styles.infos}>
                {displayTextInfos()}
                <Progress 
                    value={props.xp}
                    text={props.level}
                    max={props.maxXp}
                    reverse={props.reverse}
                />
            </div>
        </div>
    );
}

const format = (t:number):string =>
{
    const SEC = ~~(t % 60);
    const MIN = ~~(t / 60);

    return (`${MIN > 0 ? "0" + MIN : MIN}:${SEC < 10 && SEC >= 0 ? "0" + SEC : SEC}`);
};

const styles:StylesDictionary = {
    container: {
        display: "flex",
        width: "50%"
    },
    avatarBox: {
        width: 45 * 1.2,
        height: 65 * 1.2,
        border: "2px ridge white",
        boxShadow: "0px 0px 2px black inset"
    },
    infos: {
        padding: "0 5px"
    }
};