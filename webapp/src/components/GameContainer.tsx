import React from "react";
import { useSounds } from "./../hooks/useSounds";
import StylesDictionary from "./../generics/StylesDictionary";
import { AmoniteGame } from "./../games/amonite/AmoniteGame";
import { ExpanzGame } from "./../games/expanz/ExpanzGame";
import { FermeLaGame } from "./../games/ferme-la/FermeLaGame";
import { HordesGame } from "./../games/hordes/HordesGame";
import { MagmaxGame } from "./../games/magmax/MagmaxGame";
import { QuatcinelleGame } from "./../games/quatcinelle/QuatcinelleGame";
import { TrifusGame } from "./../games/trifus/TrifusGame";

type GameInstanciator = (new (s:SocketIOClient.Socket, data:any) => any);
type GameCollection = Map<string, GameInstanciator>;

interface GameContainerProps
{
    name:string;
    socket:SocketIOClient.Socket;
}

const games:GameCollection = new Map<string, GameInstanciator>([
    ["amonite", AmoniteGame],
    ["expanz", ExpanzGame],
    ["ferme-la", FermeLaGame],
    ["hordes", HordesGame],
    ["magmax", MagmaxGame],
    ["quatcinelle", QuatcinelleGame],
    ["trifus", TrifusGame]
]);

export default function GameContainer(props:GameContainerProps)
{
    const [hasStarted, setHasStarted] = React.useState(false);
    const [hasEnded, setHasEnded] = React.useState("");
    const [game, setGame] = React.useState(null) as [GameInstanciator|null, Function];
    const [playSound] = useSounds(["game_started"]);

    const start = (data:any) => {
        const ctor = games.get(props.name);

        if (!game && ctor)
            setGame(new ctor(props.socket, data));
        playSound("game_started");
        setHasStarted(true);
    };
    const end = (data:any) => {
        setHasEnded(data.hasWon ? "true" : "false");
    };
    const showBanner = () => {
        if (hasEnded === "")
            return;
        return (
            <div>
                {hasEnded}
            </div>
        );
    };

    React.useEffect(() => {
        props.socket.on("start", start);
        props.socket.on("end", end);
        return (() => {
            props.socket.off("start", start);
            props.socket.off("end", end);
        });
    });
    return (
        <div style={hasEnded !== "" ? styles.end : {}}>
            {showBanner()}
            <canvas></canvas>
        </div>
    );
}

const styles:StylesDictionary = {
    end: {
        filter: "grayscale(80%)"
    }
};