import React from "react";
import { UserContext } from "./../contexts/UserContext";
import BicoloredText from "./../generics/BicoloredText";
import Icon from "./../generics/Icon";
import StylesDictionary from "./../generics/StylesDictionary";

export default function Infobar()
{
    const context = React.useContext(UserContext);
    
    return (
        <div style={styles.infobar}>
            <BicoloredText
                style={{margin: 0}}
                text={`Bonjour ${context.username}!`}
            />
            <div style={styles.rightInfos}>
                <div style={styles.tokens}>
                    <Icon
                        url="large_freeToken.gif"
                        alt="Free token icon"
                        text={context.freeTokens}
                        size={10}
                    />
                    <Icon
                        url="large_token.gif"
                        alt="Token icon"
                        text={context.boughtTokens}
                        size={10}
                    />
                </div>
                <Icon
                    url="large_cap.gif"
                    alt="Large cap icon"
                    text={context.cjs}
                    size={20}
                    style={{margin: "0 35px"}}
                />
                <Icon
                    url="large_online.gif"
                    alt="Large online icon"
                    size={20}
                />
            </div>
        </div>
    );
}

const styles:StylesDictionary = {
    infobar: {
        display: "flex",
        justifyContent: "space-between",
        width: "100%"
    },
    rightInfos: {
        display: "flex"
    },
    tokens: {
        display: "flex",
        flexDirection: "column"
    }
}