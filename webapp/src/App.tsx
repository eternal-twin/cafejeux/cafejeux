import React from 'react';
import { Route, Switch } from "react-router-dom";
import theme from "./Theme";
import UserContextProvider from "./contexts/UserContext";
import Footer from "./Footer";
import GameInterface from "./GameInterface";
import Header from "./Header";
import Home from "./Home";
import Navbar from "./Navbar";
import StylesDictionary from "./generics/StylesDictionary";

export default function App()
{
    return (
        <div style={styles.container}>
            <UserContextProvider>
                <Header />
                <main style={styles.main}>
                    <div style={{flex: "0 0 159px"}}>
                        <Navbar />
                    </div>
                    <Switch>
                        <Route path="/games/:game">
                            <GameInterface />
                        </Route>
                        <Route path="/">
                            <Home />
                        </Route>
                    </Switch>
                </main>
                <Footer />
            </UserContextProvider>
        </div>
    );
}

const styles:StylesDictionary = {
    container: {
        backgroundColor: theme.secondary.main
    },
    main: {
        display: "flex",
        marginTop: -50,
        marginBottom: -80,
        marginLeft: 45,
        width: 924, //Temporary!
        backgroundColor: theme.secondary.light,
        borderRight: `1px ${theme.primary.light} solid`,
        borderLeft: `1px ${theme.primary.light} solid`
    }
};