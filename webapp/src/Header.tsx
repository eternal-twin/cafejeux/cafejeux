import React from "react";
import { MdVolumeUp, MdVolumeOff, MdSearch } from "react-icons/md";
import { UserContext } from "./contexts/UserContext";
import BackgroundImageContainer from "./generics//BackgroundImageContainer";
import Infobar from "./components/Infobar";
import StylesDictionary from "./generics/StylesDictionary";

interface HeaderProps
{

}

export default function Header(props:HeaderProps)
{
    const context = React.useContext(UserContext);
    
    return (
        <BackgroundImageContainer
            url="site_header.gif"
            style={styles.container}
            noCover
        >
            <BackgroundImageContainer
                url="content_header.jpg"
                style={styles.header}
            >
                <div style={styles.innerContainer}>
                    <Infobar />
                    <div style={{marginTop: 20}}>
                        <span style={styles.button}><MdSearch style={styles.icon} /> Amis</span>
                        <span style={styles.button} onClick={context.switchSound}>
                            {context.soundOn ? <><MdVolumeUp style={styles.icon} /> On </> : <><MdVolumeOff style={styles.icon} /> Off</>}
                        </span>
                    </div>
                </div>
            </BackgroundImageContainer>
        </BackgroundImageContainer>
    );
}

const styles:StylesDictionary = {
    container: {
        backgroundRepeat: "repeat-x",
        backgroundSize: "contain",
        height: 350
    },
    header: {
        display: "flex",
        flexDirection: "column",
        justifyContent: "flex-end",
        width: 1000,
        height: "100%"
    },
    innerContainer: {
        marginLeft: "auto",
        marginRight: 40,
        marginBottom: 5,
        width: "70%",
        height: "50%"
    },
    button: {
        marginLeft: 10,
        padding: 2,
        backgroundColor: "rgba(255, 255, 255, 0.1)",
        cursor: "pointer"
    },
    icon: {
        verticalAlign: "middle"
    }
};