import { FermeLaAnimals } from "./FermeLaAnimals";

export interface FermeLaTile
{
    animal?:FermeLaAnimals;
    colorAngle?:number;
}