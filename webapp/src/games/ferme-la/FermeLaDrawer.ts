import { ADrawer } from "./../ADrawer";
import { FermeLaAnimals } from "./FermeLaAnimals";
import { FermeLaGame } from "./FermeLaGame";
import { FermeLaTile } from "./FermeLaTile";
import { Mouse } from "./../Mouse";
import { Vector2 } from "./../lib/Vector2";

export class FermeLaDrawer extends ADrawer
{
    private static readonly MARGIN = 25;
    private static readonly SENSIBILITY = 0.25;
    private readonly WIDTH:number;
    private readonly HEIGHT:number;
    private scale:Vector2 = {x: 0, y: 0};

    constructor(ctx:CanvasRenderingContext2D)
    {
        super(ctx); 
        this.WIDTH = FermeLaGame.WIDTH - FermeLaDrawer.MARGIN * 2;
        this.HEIGHT = FermeLaGame.HEIGHT - FermeLaDrawer.MARGIN * 2;
    }

    private positionToScreen(pos:Vector2)
    {
        return ({
            x: pos.x * this.scale.x + FermeLaDrawer.MARGIN,
            y: pos.y * this.scale.y + FermeLaDrawer.MARGIN
        });
    }
    private drawCircle(pos:Vector2)
    {
        this.ctx.fillStyle = "white";
        this.ctx.lineWidth = 2;
        this.ctx.beginPath();
        this.ctx.arc(pos.x + 1, pos.y + 1, 2, 0, Math.PI * 2);
        this.ctx.stroke();
        this.ctx.beginPath();
        this.ctx.arc(pos.x, pos.y, 2, 0, Math.PI * 2);
        this.ctx.fill();
        this.ctx.stroke();
    }

    public init(width:number, height:number)
    {
        this.scale = {
            x: ~~(this.WIDTH / (width - 1)),
            y: ~~(this.HEIGHT / (height - 1))
        };
    }
    public preload()
    {
        const BASE = `${process.env.PUBLIC_URL}/assets/ferme-la`;
        
        this.addImage("background", `${BASE}/background.png`);
        this.addImage("foreground", `${BASE}/foreground.png`);
        this.addImage("captured", `${BASE}/captured.png`);
        this.addImage(FermeLaAnimals.CHICKEN, `${BASE}/chicken.png`);
        this.addImage(FermeLaAnimals.SHEEP, `${BASE}/sheep.png`);
        this.addImage(FermeLaAnimals.COW, `${BASE}/cow.gif`);
    }
    public update(d:number)
    {
        this.ratio += d;
    }
    public getMousePosition()
    {
        return ({
            x: (Mouse.x - FermeLaDrawer.MARGIN) / this.scale.x,
            y: (Mouse.y - FermeLaDrawer.MARGIN) / this.scale.y
        });
    }
    public getMouseIntersections():Vector2[]
    {
        const MOUSE = this.getMousePosition();
        const output:Vector2[] = [
            {x: ~~MOUSE.x, y: ~~MOUSE.y},
            {x: ~~MOUSE.x + 1, y: ~~MOUSE.y + 1}
        ];

        if (MOUSE.y > ~~MOUSE.y + (1 - FermeLaDrawer.SENSIBILITY)) {
            output[0].y++;
        }
        else if (MOUSE.y < ~~MOUSE.y + FermeLaDrawer.SENSIBILITY) {
            output[1].y--;
        }
        else if (MOUSE.x > ~~MOUSE.x + (1 - FermeLaDrawer.SENSIBILITY)) {
            output[0].x++
        }
        else if (MOUSE.x < ~~MOUSE.x + FermeLaDrawer.SENSIBILITY) {
            output[1].x--;
        }
        else
            return ([]);
        return (output);
    }
    public drawSelectedIntersection()
    {
        const MI = this.getMouseIntersections();
        
        if (MI.length === 0)
            return;
        MI[0] = this.positionToScreen(MI[0]);
        MI[1] = this.positionToScreen(MI[1]);
        this.ctx.strokeStyle = `rgba(255, 255, 255, ${Math.abs(Math.cos(this.ratio))})`;
        this.ctx.lineWidth = 3;
        this.ctx.moveTo(MI[0].x, MI[0].y);
        this.ctx.lineTo(MI[1].x, MI[1].y);
        this.ctx.stroke();
    }
    public drawTile(tile:FermeLaTile, pos:Vector2)
    {
        const P = this.positionToScreen(pos);

        if (tile.colorAngle !== undefined) {
            this.ctx.filter = `hue-rotate(${tile.colorAngle}deg)`;
            this.ctx.drawImage(this.getImage("captured")!, 
                P.x - FermeLaDrawer.MARGIN * 0.6, 
                P.y - FermeLaDrawer.MARGIN * 0.6,
                this.scale.x * 1.7, this.scale.y * 1.7
            );
            this.ctx.filter = "none";
        }
        if (tile.animal)
            this.ctx.drawImage(this.getImage(tile.animal)!, P.x, P.y);
    }
    public drawIntersection(p1:Vector2, p2:Vector2)
    {
        const sp1 = this.positionToScreen(p1);
        const sp2 = this.positionToScreen(p2);

        this.ctx.lineWidth = 3;
        this.ctx.strokeStyle = "rgb(143, 86, 1)";
        this.ctx.beginPath();
        this.ctx.moveTo(sp1.x, sp1.y);
        this.ctx.lineTo(sp2.x, sp2.y);
        this.ctx.stroke();
        this.drawCircle(sp1);
        this.drawCircle(sp2);
    }
}