import { AGame } from "./../AGame";
import { Mouse } from "./../Mouse";
import { ExpanzDrawer } from "./ExpanzDrawer";
import { ExpanzSystem } from "./ExpanzSystem";

export class ExpanzGame extends AGame<ExpanzSystem, ExpanzDrawer>
{
    constructor(socket:SocketIOClient.Socket, data:any)
    {
        super(ExpanzSystem, ExpanzDrawer, socket, data);
        Mouse.onClick(() => {
            const pos = this.helper.getMousePosition()

            this.system.emit("play", {
                position: pos
            });
        });
    }

    public update = (d:number) =>
    {
        this.system.forEach((tile) => tile.update(d));
    }
    public draw(ctx:CanvasRenderingContext2D)
    {
        const MOUSE = this.helper.getMousePosition();

        ctx.fillStyle = "black";
        ctx.fillRect(0, 0, ExpanzGame.WIDTH, ExpanzGame.HEIGHT);
        this.system.forEach((tile, x, y) => {
            tile.isSelected = false;
            if (x === MOUSE.x && y === MOUSE.y)
                tile.isSelected = true;
            this.helper.drawTile(tile);
        });
    }
}