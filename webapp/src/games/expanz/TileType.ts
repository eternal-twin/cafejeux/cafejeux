export enum TileType
{
    GRAY,
    PINK,
    YELLOW,
    GREEN,
    BLUE,
    COUNT
}