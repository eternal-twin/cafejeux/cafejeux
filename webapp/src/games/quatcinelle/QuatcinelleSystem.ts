import { AGameSystem } from "./../AGameSystem";
import { Vector2 } from "./../lib/Vector2";
import { TileType } from "./TileType";

interface Pawn
{
    pos:Vector2;
    type:TileType;
}

export class QuatcinelleSystem extends AGameSystem
{
    private static readonly dir:Vector2[] = [
        {x: -1, y: 0},
        {x: 1, y: 0},
        {x: 0, y: -1},
        {x: 0, y: 1}
    ];
    private _possibilities:Vector2[] = [];
    public readonly board:Pawn[] = [];
    public readonly winningPawns:Pawn[] = [];

    constructor(socket:SocketIOClient.Socket, data:any)
    {
        super(socket, data);
        this.append({x: 0, y: 0});
    }
    
    private isTaken(pos:Vector2):boolean
    {
        let pawn:Pawn;

        for (let i = this.board.length - 1; i >= 0; i--) {
            pawn = this.board[i];
            if (pos.x === pawn.pos.x && pos.y === pawn.pos.y)
                return (true);
        }
        return (false);
    }
    private append(pos:Vector2, isSamePlayer?:boolean)
    {
        let pawn:Pawn = {
            pos: pos,
            type: TileType.UNDEFINED
        };
        
        if (isSamePlayer !== undefined) {
            pawn.type = isSamePlayer ? TileType.RED : TileType.GREEN;
        }
        this.board.push(pawn);
        this._possibilities = this.computePossibilities();
    }
    private computePossibilities():Vector2[]
    {
        const output:Vector2[] = [];

        this.board.forEach((pawn) => {
            QuatcinelleSystem.dir.forEach((d) => {
                const pos = {
                    x: pawn.pos.x + d.x,
                    y: pawn.pos.y + d.y,
                };

                if (this.isTaken(pos))
                    return;
                output.push(pos);
            });
        });
        return (output);
    }

    protected update(data:any)
    {
        this.append(data.position, data.senderId === data.owner);
    }
    protected end(data:any)
    {
        this._possibilities = [];
        data.pawns.forEach((pawn:Vector2) => {
            this.winningPawns.push({
                pos: pawn,
                type: data.hasWon ? TileType.RED : TileType.GREEN
            });
        });
    }

    public get possibilites():Vector2[]
    {
        return (this._possibilities);
    }
}