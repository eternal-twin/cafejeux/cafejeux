import { ADrawer } from "./../ADrawer";
import { Mouse } from "./../Mouse";
import { Circle } from "./../lib/Circle";
import { HorizontalSpritesheet } from "./../lib/HorizontalSpritesheet";
import { Vector2 } from "./../lib/Vector2";
import { QuatcinelleGame as Game } from "./QuatcinelleGame";
import { TileType } from "./TileType";

export class QuatcinelleDrawer extends ADrawer
{
    private static readonly NB_SPRITES = 25;
    private static readonly SPRITE_SPEED = 1 / 25;
    private static readonly RADIUS = 15;
    private static readonly DIAMETER = QuatcinelleDrawer.RADIUS * 2;
    private static readonly SPRITE_SIZE = {
        x: QuatcinelleDrawer.DIAMETER * 1.5,
        y: QuatcinelleDrawer.DIAMETER
    };
    public static readonly NB_LAYERS = 3;
    private readonly sprites = new Map<string, HorizontalSpritesheet>();
    private readonly circle = new Circle(0, 0, QuatcinelleDrawer.RADIUS);

    private setupSpritesheets():void
    {
        let hs:HorizontalSpritesheet;
        
        this.sprites.set(TileType.RED, new HorizontalSpritesheet(
            this.getImage(TileType.RED)!,
            QuatcinelleDrawer.NB_SPRITES,
            QuatcinelleDrawer.SPRITE_SPEED
        ));
        hs = new HorizontalSpritesheet(
            this.getImage(TileType.GREEN)!,
            QuatcinelleDrawer.NB_SPRITES,
            QuatcinelleDrawer.SPRITE_SPEED
        );
        hs.currentSprite = QuatcinelleDrawer.NB_SPRITES / 2
        this.sprites.set(TileType.GREEN, hs);
    }
    
    public preload():void
    {
        const BASE = `${process.env.PUBLIC_URL}/assets/quatcinelle`;
        
        this.addImage(TileType.RED, `${BASE}/red.png`);
        this.addImage(TileType.GREEN, `${BASE}/green.png`);
        for (let i = 0; i < QuatcinelleDrawer.NB_LAYERS; i++)
            this.addImage("bg" + i, `${BASE}/bg${i + 1}.png`);
        this.setupSpritesheets();
    }
    public update(d:number):void
    {
        this.ratio += d;
        this.sprites.get(TileType.RED)?.update(d);
        this.sprites.get(TileType.GREEN)?.update(d);
    }
    public getMousePosition():Vector2
    {
        let output:Vector2 = {
            x: Mouse.x - Game.WIDTH / 2,
            y: Mouse.y - Game.HEIGHT / 2
        };

        output.x += Math.sign(output.x) * QuatcinelleDrawer.RADIUS;
        output.y += Math.sign(output.y) * QuatcinelleDrawer.RADIUS;
        output.x = ~~(output.x / QuatcinelleDrawer.DIAMETER);
        output.y = ~~(output.y / QuatcinelleDrawer.DIAMETER);
        return (output);
    }
    public drawBackground():void
    {
        this.ctx.fillStyle = "black";
        this.ctx.fillRect(0, 0, Game.WIDTH, Game.HEIGHT);
        for (let i = 0; i < QuatcinelleDrawer.NB_LAYERS; i++) {
            this.ctx.drawImage(this.getImage("bg" + i)!, 0, 0, Game.WIDTH, Game.HEIGHT);
        }
    }
    public drawPossibilities(possibilities:Vector2[])
    {
        this.ctx.fillStyle = `rgba(255, 255, 255, ${0.5 * Math.abs(Math.cos(this.ratio))})`;
        possibilities.forEach((pos) => {
            this.circle.x = pos.x * QuatcinelleDrawer.DIAMETER;
            this.circle.y = pos.y * QuatcinelleDrawer.DIAMETER;
            this.circle.draw(this.ctx, true);
        });
    }
    public drawPawn(pos:Vector2, type:TileType)
    {
        const P = {
            x: pos.x * QuatcinelleDrawer.DIAMETER,
            y: pos.y * QuatcinelleDrawer.DIAMETER
        };

        if (type === TileType.UNDEFINED) {
            this.ctx.strokeStyle = "rgba(250, 250, 250, 0.5)";
            this.ctx.lineWidth = 3;
            this.circle.x = P.x;
            this.circle.y = P.y;
            this.circle.draw(this.ctx);
        }
        else {
            P.x -= QuatcinelleDrawer.SPRITE_SIZE.x / 2;
            P.y -= QuatcinelleDrawer.SPRITE_SIZE.y / 2;
            this.sprites.get(type)?.draw(this.ctx, P, QuatcinelleDrawer.SPRITE_SIZE);
        }
    }
}