import { AGameSystem } from "./../AGameSystem";
import { AmoniteBoardType } from "./AmoniteBoardType"
import { Vector2 } from "./../lib/Vector2";

export class AmoniteSystem extends AGameSystem
{
    private readonly _board:AmoniteBoardType;
    public readonly color:string;
    
    constructor(socket:SocketIOClient.Socket, data:any)
    {
        super(socket, data);
        this.color = data.color;
        this._board = this.replaceIdsWithColors(data.board, data.senderId);
    }

    private replaceIdsWithColors(board:AmoniteBoardType, id:string):AmoniteBoardType
    {
        board.forEach((line) => {
            for (let i = line.length - 1; i >= 0; i--) {
                if (!line[i])
                    continue;
                if (id === line[i])
                    line[i] = this.color;
                else
                    line[i] = this.color === "red" ? "yellow" : "red";
            }
        });
        return (board);
    }
    public update(data:any)
    {
        this._board[data.target.x][data.target.y] = this._board[data.position.x][data.position.y];
        this._board[data.position.x][data.position.y] = undefined;
    }
    public end(data:any)
    {

    }
    public isOwned(pos:Vector2):boolean
    {
        return (this._board[pos.x]?.[pos.y] === this.color);
    }

    public get board():AmoniteBoardType
    {
        return (this._board);
    }
}