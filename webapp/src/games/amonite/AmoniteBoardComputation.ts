import { AmoniteBoardType } from "./AmoniteBoardType";
import { Vector2 } from "./../lib/Vector2";

export class AmoniteBoardComputation
{
    constructor(private readonly board:AmoniteBoardType) {}
    
    private isOutside(p:Vector2):boolean
    {
        return (p.x < 0 || p.x >= this.board.length || p.y < 0 || p.y >= this.board[p.x].length);
    }
    private getPawns(id:string):Vector2[]
    {
        let output:Vector2[] = [];
        
        for (let x = this.board.length - 1; x >= 0; x--) {
            for (let y = this.board[x].length - 1; y >= 0; y--) {
                if (this.board[x][y] === id)
                    output.push({x, y});
            }
        }
        return (output);
    }
    private getPositionsAroundTile(pos:Vector2):Vector2[]
    {
        const output:Vector2[] = [];
        const dir = [-1, 1];
        let padding:number;

        if (this.isOutside(pos))
            return ([]);
        if (pos.y - 1 >= 0)
            output.push({x: pos.x, y: pos.y - 1});
        if (pos.y < this.board[pos.x].length)
            output.push({x: pos.x, y: pos.y + 1});
        for (let i = dir.length - 1; i >= 0; i--) {
            if (pos.x + dir[i] < 0 || pos.x + dir[i] >= this.board.length)
                continue;
            padding = this.board[pos.x].length < this.board[pos.x + dir[i]].length ? 0 : -1;
            output.push({x: pos.x + dir[i], y: pos.y + padding});
            output.push({x: pos.x + dir[i], y: pos.y + 1 + padding});
        }
        return (output);
    }
    private arePositionsNextToEachOther(p1:Vector2, p2:Vector2):boolean
    {
        const positionsAround = this.getPositionsAroundTile(p1);
        
        for (let i = positionsAround.length - 1; i >= 0; i--) {
            if (p2.x === positionsAround[i].x && p2.y === positionsAround[i].y)
                return (true);
        }
        return (false);
    }
    private getReachablePositions(pos:Vector2, intermediates:Vector2[]):Vector2[]
    {
        const cpy = intermediates.slice();
        let positionsAround:Vector2[];
        let output:Vector2[] = [];

        if (cpy.length === 0)
            return ([]);
        for (let i = cpy.length - 1; i >= 0; i--) {
            if (!this.arePositionsNextToEachOther(pos, cpy[i]))
                continue;
            positionsAround = this.getPositionsAroundTile(cpy.splice(i, 1)[0]);
            for (const pa of positionsAround) {
                if (this.arePositionsNextToEachOther(pos, pa) || this.isOutside(pa))
                    continue;
                output.push(pa);
                if (!this.board[pa.x][pa.y])
                    output = output.concat(this.getReachablePositions(pa, cpy));
            };
        }
        return (output);
    }

    public getPossibilities(pos:Vector2):Vector2[]
    {
        const id:string|undefined = this.board[pos.x]?.[pos.y];
        const pawns = this.getPawns(id!).filter((p) => p.x !== pos.x || p.y !== pos.y);
        let output = this.getPositionsAroundTile(pos);

        output = output.filter((pos) =>
        {
            return (!this.board[pos.x]?.[pos.y] && !this.isOutside(pos));
        });
        output = output.concat(this.getReachablePositions(pos, pawns));
        output = output.filter((fPos, index) => {
            if (this.board[pos.x][pos.y] === this.board[fPos.x][fPos.y])
                return (false);
            return (index === output.findIndex((p) => p.x === fPos.x && p.y === fPos.y));
        });
        return (output);
    }
}