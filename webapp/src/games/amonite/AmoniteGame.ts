import { AGame } from "./../AGame";
import { AmoniteBoardComputation } from "./AmoniteBoardComputation";
import { AmoniteDrawer } from "./AmoniteDrawer";
import { AmoniteSystem } from "./AmoniteSystem";
import { Mouse } from "./../Mouse";
import { Vector2 } from "./../lib/Vector2";

export class AmoniteGame extends AGame<AmoniteSystem, AmoniteDrawer>
{
    private boardComputation:AmoniteBoardComputation;
    private selected:Vector2|undefined = undefined;
    private possibilities:Vector2[] = [];
    
    constructor(socket:SocketIOClient.Socket, data:any)
    {
        super(AmoniteSystem, AmoniteDrawer, socket, data);
        this.boardComputation = new AmoniteBoardComputation(this.system.board)
        this.helper.color = this.system.color;
        Mouse.onClick(this.onClick);
    }

    private onClick = () =>
    {
        let target:Vector2;
            
        if (!this.selected) {
            this.selected = this.helper.getMousePosition();
            if (this.system.isOwned(this.selected)) {
                this.possibilities = this.boardComputation.getPossibilities(this.selected);
                return;
            }
            this.selected = undefined;
            return;
        }
        target = this.helper.getMousePosition();
        if (!target || this.system.isOwned(target)) {
            this.selected = undefined;
            return;
        }
        this.system.emit("play", {
            position: this.selected,
            target: target
        });
        this.selected = undefined;
    }
    public draw(ctx:CanvasRenderingContext2D)
    {
        ctx.clearRect(0, 0, AmoniteGame.WIDTH, AmoniteGame.HEIGHT);
        this.helper.drawBackground();
        this.helper.drawBoard(this.system.board);
        if (!this.selected)
            return;
        this.helper.drawSelected(this.selected, this.system.board[this.selected.x]?.length);
        this.helper.drawPossibilities(this.system.board, this.possibilities);
    }
}