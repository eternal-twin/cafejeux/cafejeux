import { Vector2 } from "./Vector2";

export class HorizontalSpritesheet
{
	private _currentSprite:number = 0;
	private _mustRunOnce = false;
	public position = {x: 0, y: 0};
	public size = {x: 0, y: 0};

	constructor(
		private image:HTMLImageElement,
		private readonly NB_SPRITES:number,
		private readonly SPEED:number
	)
	{

	}

	public runOnce()
	{
		this._mustRunOnce = true;
		this._currentSprite = 0;
	}
	public update(delta:number)
	{
		this._currentSprite += delta / this.SPEED;
		if (this.hasAnimationEnded)
			return;
		if (this._currentSprite >= this.NB_SPRITES)
			this._currentSprite = 0;
	}
	public draw(ctx:CanvasRenderingContext2D, pos:Vector2 = this.position, size:Vector2 = this.size)
	{
		const W = this.image?.width / this.NB_SPRITES;
		const SX = W * (~~this._currentSprite);

		if (this.image && !this.hasAnimationEnded)
			ctx.drawImage(this.image, SX, 0, W, this.image.height, pos.x, pos.y, size.x, size.y);
	}
	public get hasAnimationEnded()
	{
		return (this._mustRunOnce && this._currentSprite >= this.NB_SPRITES);
	}
	public set currentSprite(cs:number)
	{
		this._currentSprite = cs;
	}
}