export abstract class AShape
{
    public draw(ctx:CanvasRenderingContext2D, isFilled?:boolean)
    {
        if (isFilled === undefined) {
            ctx.fill();
            ctx.stroke();
        }
        else
            isFilled ? ctx.fill() : ctx.stroke();
    }
}