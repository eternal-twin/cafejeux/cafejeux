import { AShape } from "./AShape";
import { Vector2 } from "./../lib/Vector2";

export class Triangle extends AShape
{
    constructor(
        public a:Vector2 = {x: 0, y: 0}, 
        public b:Vector2 = {x: 0, y: 0}, 
        public c:Vector2 = {x: 0, y: 0}
    )
    {
        super();
    }

    public static getArea(a:Vector2, b:Vector2, c:Vector2):number
    {
        return ((a.x - c.x) * (b.y - c.y) - (b.x - c.x) * (a.y - c.y));
    }
    public isPointInside(x:number|Vector2, y:number = 0):boolean
    {
        const v:Vector2 = (typeof x === "number") ? {x, y} : x;
        const area1 = Triangle.getArea(v, this.a, this.b);
        const area2 = Triangle.getArea(v, this.b, this.c);
        const area3 = Triangle.getArea(v, this.c, this.a);
        const hasNegative = (area1 < 0 || area2 < 0 || area3 < 0);
        const hasPositive = (area1 > 0 || area2 > 0 || area3 > 0); 
        
        return (!(hasNegative && hasPositive));
    }
    public draw(ctx:CanvasRenderingContext2D, isFilled?:boolean)
    {
        ctx.beginPath();
        ctx.moveTo(this.a.x, this.a.y);
        ctx.lineTo(this.b.x, this.b.y);
        ctx.lineTo(this.c.x, this.c.y);
        ctx.closePath();
        super.draw(ctx, isFilled);
    }
}