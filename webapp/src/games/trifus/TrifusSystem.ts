import { AGameSystem } from "./../AGameSystem";
import { LinearMatrix } from "./../lib/LinearMatrix";
import { ITrifusCard } from "./ITrifusCard";
import { TrifusCard } from "./TrifusCard";
import { TrifusColor } from "./TrifusColor";
import { Vector2 } from "./../lib/Vector2";

type UpdatedCard = {position:Vector2, card:ITrifusCard};

export class TrifusSystem extends AGameSystem
{
    private board:LinearMatrix<TrifusCard>;
    public readonly deck:TrifusCard[] = [];
    public readonly color:TrifusColor;

    constructor(socket:SocketIOClient.Socket, data:any)
    {
        super(socket, data);
        data.board.data = TrifusSystem.convertDataToArray(data.board.data);
        data.deck = TrifusSystem.convertDataToArray(data.deck);
        this.board = new LinearMatrix(data.board);
        this.deck = data.deck.sort((a:ITrifusCard, b:ITrifusCard) => {
            return (a.strength - b.strength);
        });
        this.color = data.color;
    }
    
    private static convertDataToArray(data:ITrifusCard[]):(TrifusCard|undefined)[]
    {
        const output:(TrifusCard|undefined)[] = [];

        for (let i = 0, len = data.length; i < len; i++) {
            if (data[i])
                output.push(new TrifusCard(data[i]));
            else
                output.push(undefined);
        }
        return (output);
    }

    protected update(data:any):void
    {
        let card:TrifusCard|undefined = undefined;

        data.cards.forEach((uc:UpdatedCard) => {
            this.board?.set(uc.position.x, uc.position.y, new TrifusCard(uc.card));
            if (card)
                return;
            for (let i = this.deck.length - 1; i >= 0; i--) {
                if (this.deck[i].strength === uc.card.strength
                    && this.deck[i].type === uc.card.type) {
                    card = this.deck.splice(i, 1)[0];
                    break;
                }
            }
        });
    }
    protected end(data:any):void
    {
        console.log(data);
    }

    public getWidth():number
    {
        return (this.board?.WIDTH!);
    }
    public getHeight():number
    {
        return (this.board?.HEIGHT!);
    }
    public getCard(x:number, y:number):TrifusCard|undefined
    {
        return (this.board?.get(x, y));
    }
}