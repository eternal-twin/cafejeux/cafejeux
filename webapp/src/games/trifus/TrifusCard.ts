import { Rectangle } from "./../lib/Rectangle";
import { ITrifusCard } from "./ITrifusCard";
import { TrifusColor } from "./TrifusColor";
import { TrifusType } from "./TrifusType";
import { Vector2 } from "./../lib/Vector2";

export class TrifusCard extends Rectangle
{
    private static readonly CARD_SIZE_RATIO = {x: 0.8, y: 0.85};
    private static readonly BORDER_SIZE = 4;
    private static readonly CORNER = 5;
    private static readonly SYMBOL_SCALE = 0.85;
    private static readonly SELECTION_ELEVATION = 5;
    private static readonly COLORS = new Map([
        ["blue", {fill: "rgb(105, 73, 159)", stroke: "rgb(198, 157, 242)"}],
        ["red", {fill: "rgb(99, 109, 0)", stroke: "rgb(168, 167, 49)"}],
        [undefined, {fill: "rgb(88, 88, 88)", stroke: "rgb(177, 177, 177)"}]
    ]);
    public static readonly size:Vector2 = {x: 0, y: 0};
    public static readonly imgSize:Vector2 = {x: 0, y: 0};
    public static image?:HTMLImageElement;
    
    public readonly strength:number;
    public readonly type:TrifusType;
    public owner:TrifusColor;
    public isSelected:boolean = false;

    constructor(card:ITrifusCard)
    {
        super(0, 0, TrifusCard.size.x, TrifusCard.size.y, TrifusCard.CORNER);
        this.strength = card.strength;
        this.type = card.type;
        this.owner = card.owner;
    }

    public static init(scale:Vector2)
    {
        TrifusCard.size.x = scale.x * TrifusCard.CARD_SIZE_RATIO.x - TrifusCard.BORDER_SIZE;
        TrifusCard.size.y = scale.y * TrifusCard.CARD_SIZE_RATIO.y - TrifusCard.BORDER_SIZE;
        TrifusCard.imgSize.x = TrifusCard.size.x * TrifusCard.SYMBOL_SCALE;
        TrifusCard.imgSize.y = TrifusCard.size.y * TrifusCard.SYMBOL_SCALE;
    }

    private drawText(ctx:CanvasRenderingContext2D)
    {
        ctx.lineWidth = 1;
        ctx.strokeStyle = "white";
        ctx.strokeText(
            this.strength.toString() || "",
            this.x + TrifusCard.BORDER_SIZE,
            this.y + TrifusCard.BORDER_SIZE * 3.5,
        );
    }
    
    public draw(ctx:CanvasRenderingContext2D)
    {
        this.w = TrifusCard.size.x;
        this.h = TrifusCard.size.y;
        ctx.fillStyle = TrifusCard.COLORS.get(this.owner)!.fill;
        ctx.strokeStyle = TrifusCard.COLORS.get(this.owner)!.stroke;
        ctx.lineWidth = TrifusCard.BORDER_SIZE;
        if (this.isSelected)
            this.y -= TrifusCard.SELECTION_ELEVATION;
        super.draw(ctx);
        ctx.drawImage(
            TrifusCard.image!,
            this.x + (this.w - TrifusCard.imgSize.x) * 0.5, 
            this.y + (this.h - TrifusCard.imgSize.y) * 0.5, 
            TrifusCard.imgSize.x, TrifusCard.imgSize.y
        );
        this.drawText(ctx);
        if (this.isSelected)
            this.y += TrifusCard.SELECTION_ELEVATION;
    }
    public toData():ITrifusCard
    {
        return ({
            strength: this.strength,
            type: this.type,
            owner: this.owner
        });
    }
}