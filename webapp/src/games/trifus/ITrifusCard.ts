import { TrifusColor } from "./TrifusColor";
import { TrifusType } from "./TrifusType";

export interface ITrifusCard
{
    strength:number;
    type:TrifusType;
    owner?:TrifusColor;
}