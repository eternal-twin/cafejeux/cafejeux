import { Vector2 } from "./lib/Vector2";

type KeyType = string|number;

export abstract class ADrawer
{
    private readonly imageStorage = new Map<KeyType, HTMLImageElement>();
    protected ratio = 0;

    constructor(protected ctx:CanvasRenderingContext2D)
    {

    }
    
    public abstract preload():void;
    public abstract getMousePosition():Vector2;
    public update(d:number)
    {
        this.ratio += d;
    }
    public addImage(name:KeyType, url:string):void
    {
        const img = new Image();

        img.src = url;
        this.imageStorage.set(name, img);
    }
    public drawImage(img:string, x:number, y:number, w?:number, h = w)
    {
        const image = this.getImage(img);
        
        if (!image)
            return;
        if (!w || !h)
            this.ctx.drawImage(image, x, y);
        else
            this.ctx.drawImage(image, x, y, w, h);
    }
    public getImage(name:KeyType):HTMLImageElement|undefined
    {
        return (this.imageStorage.get(name));
    }
}