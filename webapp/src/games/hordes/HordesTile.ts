import { Vector2 } from "../lib/Vector2";
import { HordesDrawer } from "./HordesDrawer";

export type RawTile = {
    quantity:number,
    owner?:string,
    color?:string
};

export class HordesTile
{
    public static readonly TILE_SLOPE_RATIO = (1 / 4);
    public owner?:string;
    public color?:string;
    public quantity:number = 0;
    public scale:Vector2 = {x: 0, y: 0};
    public position:Vector2 = {x: 0, y: 0};
    public canBePopulated = true;

    constructor(data:RawTile|undefined)
    {
        if (!data) {
            this.canBePopulated = false;
            return;
        }
        this.quantity = data.quantity;
        this.owner = data.owner;
        this.color = data.color;
    }
    private drawHexagonFromPosition(ctx:CanvasRenderingContext2D, pos:Vector2)
    {
        const SLOPE = this.scale.y * HordesTile.TILE_SLOPE_RATIO;

        ctx.beginPath();
        ctx.moveTo(pos.x + this.scale.x / 2, pos.y);
        ctx.lineTo(pos.x, pos.y + SLOPE);
        ctx.lineTo(pos.x, pos.y + this.scale.y - SLOPE);
        ctx.lineTo(pos.x + this.scale.x / 2, pos.y + this.scale.y);
        ctx.lineTo(pos.x + this.scale.x, pos.y + this.scale.y - SLOPE);
        ctx.lineTo(pos.x + this.scale.x, pos.y + SLOPE);
        ctx.closePath();
        ctx.fill();
    }
   
    public computePosition():Vector2
    {
        return ({
            x: this.position.x * this.scale.x + HordesDrawer.MARGIN.x + (this.position.y % 2) * this.scale.x / 2,
            y: this.position.y * this.scale.y + HordesDrawer.MARGIN.y - this.position.y * this.scale.y * HordesTile.TILE_SLOPE_RATIO
        });
    }
    public drawBuilding(ctx:CanvasRenderingContext2D, image:HTMLImageElement|undefined)
    {
        const pos = this.computePosition();
        const Y = this.position.y * this.scale.y * HordesTile.TILE_SLOPE_RATIO;

        if (!image)
            return;
        ctx.drawImage(image,
            pos.x - (this.scale.x * 1.15 * 0.1) / 2,
            this.position.y * this.scale.y + HordesDrawer.MARGIN.y - Y - HordesDrawer.MARGIN.x / 2,
            this.scale.x * 1.15, this.scale.y * 1.15
        );
    }
    public drawPeople(ctx:CanvasRenderingContext2D, image:HTMLImageElement|undefined)
    {
        const pos = this.computePosition();

        if (!image)
            return;
        if (this.owner) {
            ctx.fillStyle = this.color === "red" ? "rgba(0, 0, 128, 0.2)" : "rgba(128, 0, 0, 0.2)";
        }
        else {
            ctx.fillStyle =  "rgba(64, 0, 0, 0.2)";
        }
        this.drawHexagonFromPosition(ctx, pos);
        ctx.drawImage(image,pos.x + (this.scale.x - image.width) / 2, pos.y + (this.scale.y - image.height) / 2);
    }
    public draw(ctx:CanvasRenderingContext2D,  image:HTMLImageElement|undefined)
    {
        const pos = this.computePosition();

        if (!image)
            return;
        ctx.drawImage(image, pos.x, pos.y, this.scale.x, this.scale.y);
    }
};