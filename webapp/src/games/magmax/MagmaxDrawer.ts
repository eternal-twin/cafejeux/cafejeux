import { ADrawer } from "./../ADrawer";
import { HorizontalSpritesheet } from "./../lib/HorizontalSpritesheet";
import { MagmaxBoard } from "./MagmaxBoard";
import { MagmaxGame } from "./MagmaxGame";
import { MagmaxOrientation } from "./MagmaxOrientation";
import { MagmaxPawn } from "./MagmaxPawn";
import { MagmaxTile } from "./MagmaxTile";
import { Mouse } from "./../Mouse";
import { Rectangle } from "./../lib/Rectangle";
import { Vector2 } from "./../lib/Vector2";

export class MagmaxDrawer extends ADrawer
{
    private static readonly CARD_NAMES = [
        "move_single", "slide_single", "move_all", 
        "place_bomb","place_rock", "shoot_laser", 
        "shoot_light", "shoot_water", "show_bombs",
        "protect_single", "end_turn"
    ];
    private static readonly MAX_NB_CARDS = 6;
    private static readonly PADDING = 60;
    private static readonly DECK_PADDING = MagmaxDrawer.PADDING * 0.33;
    private static readonly CARD_HEIGHT_RATIO = 0.85;
    private board = new MagmaxBoard();
    private sprites = new Map<string, HorizontalSpritesheet>();
    private card:Rectangle;
    private orientation:MagmaxOrientation = MagmaxOrientation.SOUTH;
    
    constructor(ctx:CanvasRenderingContext2D)
    {
        super(ctx);
        this.card = new Rectangle(0, 0, 0, 0);
        this.card.w = (MagmaxGame.WIDTH - MagmaxDrawer.DECK_PADDING * 2) / MagmaxDrawer.MAX_NB_CARDS - 5;
        this.card.h = MagmaxDrawer.PADDING * MagmaxDrawer.CARD_HEIGHT_RATIO;
    }
    private drawCard(card:string)
    {
        this.card.drawImage(this.ctx, this.getImage(card)!);
        this.ctx.fillStyle = "rgba(0, 0, 0, 0)";
        if (this.card.isPointInside(Mouse.x, Mouse.y)) {
            this.ctx.fillStyle = `rgba(255, 255, 255, ${Math.abs(Math.cos(this.ratio)) * 0.25})`;
            this.card.triangles.filter((t) => t.isPointInside(Mouse.x, Mouse.y))[0].draw(this.ctx, true);
        }
        this.card.draw(this.ctx, true);
    }
    private drawSelectedPawn(pawn:MagmaxPawn)
    {
        const POS = this.board.convertRelativePosition(pawn.position);
        const SIZE = {
            x: this.board.scale.x / 2,
            y: this.board.scale.y
        };
        
        POS.x += (this.board.scale.x - SIZE.x) / 2;
        POS.y -= this.board.scale.y / 3;
        this.sprites.get("current")?.draw(this.ctx, POS, SIZE);
    }
    
    public init(w:number, h:number, color:string)
    {
        const IS_ON_TOP = color === "blue";
        const Y = (IS_ON_TOP ? 0 : MagmaxGame.HEIGHT - MagmaxDrawer.PADDING);
        
        this.board.position.x = MagmaxDrawer.PADDING * 0.5;
        this.board.position.y = IS_ON_TOP ? MagmaxDrawer.PADDING : 0;
        this.board.size.x = MagmaxGame.WIDTH - MagmaxDrawer.PADDING;
        this.board.size.y = MagmaxGame.HEIGHT - MagmaxDrawer.PADDING;
        this.board.scale.x = this.board.size.x / w;
        this.board.scale.y = this.board.size.y / h;
        this.card.y = MagmaxDrawer.PADDING * (1 - MagmaxDrawer.CARD_HEIGHT_RATIO) / 2 + Y;
    }
    public preload()
    {
        const BASE = `${process.env.PUBLIC_URL}/assets/magmax`;

        this.addImage("background", `${BASE}/background.png`);
        this.addImage("rock", `${BASE}/rock.png`);
        this.addImage("bomb", `${BASE}/bomb.png`);
        this.addImage("character", `${BASE}/character.png`);
        this.addImage("protected", `${BASE}/protected.png`);
        this.addImage("current", `${BASE}/current.png`);
        for (const card of MagmaxDrawer.CARD_NAMES) {
            this.addImage(card, `${BASE}/cards/${card}.png`);
        }
        this.sprites.set("character", new HorizontalSpritesheet(this.getImage("character")!, 30, 0.1));
        this.sprites.set("protected", new HorizontalSpritesheet(this.getImage("protected")!, 36, 0.1));
        this.sprites.set("current", new HorizontalSpritesheet(this.getImage("current")!, 16, 0.05));
    }
    public update(d:number)
    {
        this.ratio += d;
        this.sprites.forEach((sprite) => sprite.update(d));
    }
    public getMousePosition():Vector2
    {
        return (this.board.convertAbsolutePosition({x: Mouse.x, y: Mouse.y}));
    }
    public drawBackground()
    {
        this.drawImage("background",
            this.board.position.x, this.board.position.y,
            this.board.size.x, this.board.size.y
        );
    }
    public drawTile(tile:MagmaxTile, pos:Vector2)
    {
        const POS = this.board.convertRelativePosition(pos);
        const SIZE = {
            x: this.board.scale.x,
            y: this.board.scale.y
        };

        if (tile.hasRock) {
            this.drawImage("rock", POS.x, POS.y, SIZE.x, SIZE.y);
        }
        if (tile.hasBomb) {
            this.drawImage("bomb", POS.x, POS.y, SIZE.x, SIZE.y);
        }
    }
    public drawPawns(pawns:MagmaxPawn[])
    {
        pawns.forEach((pawn) => {
            const POS = this.board.convertRelativePosition(pawn.position);
            const spriteName = pawn.isProtected ? "protected" : "character";

            if (pawn.isDead)
                return;
            if (pawn.color === "red")
                this.ctx.filter = "hue-rotate(180deg)";
            this.sprites.get(spriteName)?.draw(this.ctx, POS, this.board.scale);
            this.ctx.filter = "hue-rotate(0)";
            if (pawn.isSelected)
                this.drawSelectedPawn(pawn);
        });
    }
    public drawDeck(cards:string[])
    {
        this.card.x = MagmaxDrawer.DECK_PADDING + (this.card.w + 5) * cards.length;
        if (cards.length < MagmaxDrawer.MAX_NB_CARDS)
            this.drawCard("end_turn");
        for (let i = cards.length - 1; i >= 0; i--) {
            this.card.x -= (this.card.w + 5);
            this.drawCard(cards[i]);
        }
    }
    public getCardIdFromMouse():number
    {
        const X = Mouse.x - MagmaxDrawer.DECK_PADDING;
        
        if (Mouse.y < this.card.y || X < 0)
            return (-1);
        return (~~(X / (this.card.w + 5)));
    }
    public getCardOrientation():MagmaxOrientation|undefined
    {
        let t;
        let output = MagmaxOrientation.SOUTH;

        this.card.x = MagmaxDrawer.DECK_PADDING + (this.card.w + 5) * this.getCardIdFromMouse();
        t = this.card.triangles.filter((t) => t.isPointInside(Mouse.x, Mouse.y))[0];
        if (!t)
            return (undefined);
        output = (t.a.y < t.b.y && t.a.y === t.c.y) ? MagmaxOrientation.NORTH : output;
        output = (t.a.x > t.b.x && t.a.x === t.c.x) ? MagmaxOrientation.EAST : output;
        output = (t.a.x < t.b.x && t.a.x === t.c.x) ? MagmaxOrientation.WEST : output;
        this.orientation = output;
        return (output);
    }
}