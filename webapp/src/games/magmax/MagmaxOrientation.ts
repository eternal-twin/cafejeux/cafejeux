export enum MagmaxOrientation
{
    WEST = "west",
    EAST = "east",
    SOUTH = "south",
    NORTH = "north"
}