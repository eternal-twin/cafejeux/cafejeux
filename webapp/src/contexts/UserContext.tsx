import React from "react";

interface UserContextData
{
    username:string;
    cjs:number;
    freeTokens:number;
    boughtTokens:number;
    soundOn:boolean;
    switchSound:() => void;
}

export const UserContext:React.Context<UserContextData> = React.createContext({}) as React.Context<any>;

export default class UserContextProvider extends React.Component
{
    state = {
        cjs: 3,
        freeTokens: 3,
        boughtTokens: 0,
        username: "",
        soundOn: this.getFromLocalStorage()
    }

    getFromLocalStorage():boolean
    {
        const soundOn = localStorage.getItem("soundOn");

        if (soundOn === undefined)
            return (true);
        return (soundOn === "true");
    }
    switchSound = () =>
    {
        this.setState({soundOn: !this.state.soundOn});
        localStorage.setItem("soundOn", this.state.soundOn.toString());
    }
    render()
    {
        return (
            <UserContext.Provider value={{
                ...this.state,
                switchSound: this.switchSound
            }}>
                {this.props.children}
            </UserContext.Provider>
        );
    }
}