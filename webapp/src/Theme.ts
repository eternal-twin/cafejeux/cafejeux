const theme = {
    primary: {
        main: "rgb(169, 104, 56)",
        light: "rgb(221, 166, 119)",
        dark: "rgb(117, 61, 58)"
    },
    secondary: {
        light: "rgb(65, 57, 68)",
        main: "rgb(44, 34, 52)"
    }
};

export default theme;