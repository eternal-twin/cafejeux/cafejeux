import React from "react";
import StylesDictionary from "./StylesDictionary";

interface IconProps
{
    url:string;
    alt:string;
    route?:string;
    text?:string|number;
    size:number;
    style?:React.CSSProperties;
}

export default function Icon(props:IconProps)
{
    let route = props.route || process.env.PUBLIC_URL + "/img/icons/";
    
    return (
        <div style={{...props.style, display: "inline-block"}}>
            <div style={styles.container}>
                <img
                    src={`${route}${props.url}`}
                    alt={props.alt}
                    width={props.size * 2}
                />
                <span style={{
                    fontSize: (props.size / 10) + "em",
                    marginLeft: props.size / 5
                }}>
                    {props.text}
                </span>
            </div>
        </div>
    );
}

const styles:StylesDictionary = {
    container: {
        display: "flex",
        alignItems: "center",
    }
};